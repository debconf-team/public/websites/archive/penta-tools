# Richard Darst, June 2010

"""Format:
#Where: true
#Subject: DebConf11: Added to Professional category

Optional files:
#PentaIDs:
#Filter:


"""


import mailbox
import string
import sys
import os
import re

import emailutil
from emailutil import Message, Email
import dc_objs

templateFname = sys.argv[1]
basename, ext = os.path.splitext(templateFname)
template = open(templateFname).read()

template_new = [ ]
headers = { }
for line in template.split('\n'):
    if template_new:
        template_new.append(line)
    elif not line and not template_new:
        # skip blank lines before template body starts
        continue
    elif line and line[0] == '#':
        line = line[1:].strip()
        headers[line.split(':',1)[0].strip()] = line.split(':',1)[1].strip()
    else:
        template_new.append(line)
print headers
template = "\n".join(template_new)

People = dc_objs.get_people(where=headers['Where'])
del headers['Where']

if 'Filter' in headers:
    def f(P):
        from dc_objs import CAT_B, CAT_BA, CAT_BF, CAT_BAF, CAT_PRO, CAT_COR, CAT_NONE
        from dc_objs import CAT_SPONS, CAT_PAID, CAT_UNSPONS
        from dc_objs import DC_PLAN, DC_NOPLAN, DC_NO, DC_YES
        from dc_objs import FOOD_REG, FOOD_VEG, FOOD_VEGAN, FOOD_OTHER, FOOD_NO
        from dc_objs import ACCOM_NO
        #print eval(headers['Filter'])
        return eval(headers['Filter'])

    People = filter(f, People)
    del headers['Filter']

# Include only Penta IDs found in this header
if 'PentaIDs' in headers:
    PentaIDs = headers['PentaIDs']
    PentaIDs = re.split('[, ]+', PentaIDs)
    PentaIDs = set(int(x) for x in PentaIDs if x.strip())
    People = [P for P in People if P.person_id in PentaIDs]
    del headers['PentaIDs']
# Include only Penta IDs found in this file
if 'PentaIDsFile' in headers:
    PentaIDs = open(headers['PentaIDsFile']).read()
    PentaIDs = re.split('[^0-9]+', PentaIDs)
    PentaIDs = set(int(x) for x in PentaIDs if x.strip())
    People = [P for P in People if P.person_id in PentaIDs]
    del headers['PentaIDsFile']
# Remove all Penta IDs found in this header
if 'NotPentaIDs' in headers:
    PentaIDs = headers['NotPentaIDs']
    PentaIDs = re.split('[, ]+', PentaIDs)
    PentaIDs = set(int(x) for x in PentaIDs if x.strip())
    People = [P for P in People if P.person_id not in PentaIDs]
    del headers['NotPentaIDs']
# Remove all Penta IDs found in this filename
if 'NotPentaIDsFile' in headers:
    PentaIDs = open(headers['NotPentaIDsFile']).read()
    PentaIDs = re.split('[^0-9]+', PentaIDs)
    PentaIDs = set(int(x) for x in PentaIDs if x.strip())
    People = [P for P in People if P.person_id in PentaIDs]
    del headers['NotPentaIDsFile']


# Print out summary of who gets what
for P in People:
    print P.regSummary(), P.personHeader()
print "Total people:", len(People)


# See if there are unknown headers in the file:
if set(headers.keys()) > set(('Subject', )):
    print
    print "Error: Unknown headers detected:"
    print set(headers.keys()) - set(('Subject', ))
    sys.exit(1)

#sys.exit()

mbox = mailbox.mbox(basename+'.mbox')
mbox.clear() ; mbox.flush()

for P in People:

    Body = template % P.__dict__
    
    msg = emailutil.Message(
        From=Email("DebConf Team", "registration@debconf.org"),
        To=Email(P.name, P.email),
        Subject=headers['Subject'],
        Body=Body,
        extraMsgid="".join(x for x in basename
                           if x in string.ascii_letters+string.digits),
        ReplyTo=Email("DebConf Team", "registration@debconf.org"),
        Cc=Email("DebConf Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))
