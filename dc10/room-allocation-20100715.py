# -*- coding: utf-8 -*-
# Richard Darst, May 2010
# Ana Guerrero, July 2010

from emailutil import Email, Message
import mailbox
import sys
sys.path.append('.')

import emailutil
print emailutil.__file__

body = """Hello %(name)s,

You had requested an on-campus room for accommodations during DebConf10
(either sponsored, paying for night or as professional/corporate).
We are glad to say that your room has been reserved and provisional
room assignments made.

You have been assigned to room %(room)s in the %(building)s residence hall
from %(arrival)s to %(departure)s. You room is %(room_type)s.

%(roomie_status)s

The addresses of the residence halls are:

Furnald:
2940 Broadway
New York NY 10025

Carman:
545 W. 114th St.
New York, NY 10027


Note: You will check in at Carman hall regardless of where you are assigned.
Both buildings should be entered from the Columbia superblock, not the street.
Please see our travel information pack as arrival date gets closer for full details.

For some information and links to pages for these buildings, please see
  http://wiki.debconf.org/wiki/DebConf10/Resources#Accommodations

If there is something wrong in this email, please let us know answering
this email.

Thank you,

--
Ana Guerrero
On behalf of the DebConf team
"""


if len(sys.argv) < 3:
    print "Usage: %s <input_csv> <output_mbox>"%sys.argv[0]
    exit(1)
datfile = open(sys.argv[1])
mbox = mailbox.mbox(sys.argv[2])

shared_rooms = {}

# Make dictionary with all the roomies, the key is the room 
for line in datfile:
    d = line.split(',') #d[2] is room number and d[4] person

    if d[3] == 'D': # if it is in doble room
        p = [] if (d[2] not in shared_rooms) else shared_rooms[d[2]]
        p.append(d[4]) # append new person in that room
        shared_rooms[d[2]] = p

datfile = open(sys.argv[1])
for line in datfile:
    penta_id, building, room, room_type, name, email, arrival, departure = \
           line.strip().split(',')

    # Set full word for Single/Double and building name
    room_type = 'Single' if room_type == 'S' else 'Double'
    building = 'Carman' if building == 'C' else 'Furnald'

    if room_type == 'Double' and (room in shared_rooms):
        people = shared_rooms[room]
        if len(people) <= 1:
            roomie_status = "Your room is double but you are not sharing it."
        else:
            roomie_status = "The room is being shared by "+people[0]+" and "+people[1]+"."
    else:
        roomie_status = ""


    Body = body%locals()


    msg = Message(
        # The Email(real_name, email_address) object aids in
        # making email addresses.  If you don't have a real name,
        # use it as Email(None, email_address)
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(name, email),
        Subject="DebConf10: Room allocation details",
        Body=Body,
        extraMsgid="dc10roomallocation",  # some string to put in msgid
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        # Anything can besides "From" can be a python list of Email objects.
        Cc=[Email("DebConf Registration Team", "registration@debconf.org"),],
        )
    mbox.add(str(msg))
    # Look at the mbox to see if everything looks right.  Send with
    # formail -s /usr/sbin/sendmail -ti < emailutil-test.mbox
