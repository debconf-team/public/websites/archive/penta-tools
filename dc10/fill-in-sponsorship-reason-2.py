# Richard Darst, May 2010

import mailbox
from textwrap import dedent

import dc_objs
import emailutil
from emailutil import Email, Message


body_fill_in_more = """\
Hello %(name)s,

The DebConf team is reviewing the attendees signed up for food or
accommodation sponsorship.  At this time, we can't promise food and
accommodation sponsorship to you. We will make sponsorship decisions
as soon as we can.

In order to help us make sponsorship decisions, could you please go to
  https://penta.debconf.org/penta/submission/dc10/person
and
  Penta -> Travel -> 'What are you doing for Debian?'
and fill in details of your work relevant to Debian (on Debian itself
or in the broader free software community).

Again, be broad here!  We want as many interested community members to
attend, so let us know what your other projects are.  If you are just
interested in Debian, that's fine too, but please let us know your
specific interests.

If you are in any way connected to Debian, we'll do our best to make
sure that you get sponsorship!

If you are not granted sponsorship, you can still attend.  You will
simply be asked to pay the actual costs of attendance, $53/night for
accommodation and $10-$15 per meal.

After you fill this information, we will evaluate it and we will be
able to tell you if you have been granted food and accommodation
sponsorship.

If we do not hear from you before June 10, you won't be granted
sponsorship.

If you have any questions, please contact us via IRC:
  #debconf-team @ irc.oftc.net (real-time questions)
or mailing list:
  debconf-team@lists.debconf.org (publically archived mail list)
or non-public email:
  registration@debconf.org

-- 
The DebConf team
"""

body_yes = """\
"""

body_no = """\
"""


people = dc_objs.get_people()
mbox = mailbox.mbox("dc10-fillinsponsorship-2.mbox")
mbox.clear() ; mbox.flush()

execfile(fill-in-sponsorship-reason-2.dat)

for person in people:
    # skip unsponsored and prof/corp.
    if person.person_id not in penta_ids:
        continue

    Body = body_fill_in_more % person.__dict__
    
    msg = emailutil.Message(
        From=Email("DebConf Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf10: Please fill in sponsorship information (second notice)",
        Body=Body,
        extraMsgid="dc10fillinsponsorship2",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))
