# Richard Darst, June 2010

# TODO: put the code below into a function instead of globals.

import collections
import os

import readsxc

# Find where attendee-payments.ods is stored on your system.
filenames = ["../debconf-team/dc13/accounting/attendee-payments.ods",
             "../../debconf-team/dc13/accounting/attendee-payments.ods",
             "~/debconf/debconf-team/dc13/accounting/attendee-payments.ods",
             ]  # Add your filenames here to find it on your computer

def get_payments():

    for fname in filenames:
        fname = os.path.expanduser(fname)
        if os.access(fname, os.F_OK):
            break
    else:
        # No filename detected.
        return None
    
    data = readsxc.OOSpreadData(fname)
    
    payments = collections.defaultdict(lambda: {'amount_paid':0.0, 'name':None,
                                                'dates':[]})

    names = None
    for row in data:
        if not names:
            names = [ n.lower().replace(' ', '_').replace('-', '_') for n in row ]
            continue
        if row[0] == "END":
            break

        row = dict(zip(names, row))
        # Skip rows which don't have a penta-id yet.
        if not row['penta_id'].strip():
            continue
        penta_id = int(row['penta_id'])
        amount_paid = row['amount_paid']
        amount_paid = amount_paid.strip('$ ')
        #print amount_paid
        # @@#$@$#$ this is a europen-style string rep of a number@@
        if not amount_paid:
            amount_paid = 0
        elif amount_paid[-3] == ',':
            # European-style:
            amount_paid = amount_paid.replace('.', '').replace(',', '.')
        else:
            # US-style:
            amount_paid = amount_paid.strip(' $').replace(',', '')
        payments[penta_id]['amount_paid'] += float(amount_paid)
        payments[penta_id]['name'] = row['name']
        payments[penta_id]['dates'].append(row['date_received'])
        if row['promise']:
            payments[penta_id]['promise'] = True


    payments = dict(payments) # Make it not a defaultdict anymore

    return payments

if __name__ == "__main__":
    payments = get_payments()
    for id, value in sorted(payments.iteritems()):
        print id, value
