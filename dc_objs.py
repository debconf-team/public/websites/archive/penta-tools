# Richard Darst, April 2010; Giacomo Catenazzi, June 2013
# -*- coding: UTF-8 -*-

import collections
import datetime
import time
import math

import dc_util
from dc_util import NotAnError, parseDate

import dc_config
from dc_config import Y, N, DCN, dcN, DebConfN, conference_id, DEBCAMP_START, DEBCONF_START, DEBCONF_EARLIEST_ARRIVAL, DEBCONF_END

class EmailError(Exception):
    pass


_db = None
def db():
    import pentaconnect
    global _db
    if _db is None:
        _db = pentaconnect.get_conn()
    return _db



class Person(object):
    def __init__(self, ratings=None, ratings_remarks=None, **kwargs):
        """
        """
        #print kwargs['person_id']
        self.errors = [ ]
        # Set attributes on self for all pentabarf variables:
        for k, v in kwargs.iteritems():
            if isinstance(v, (str,unicode)):
                v = v.strip() # strip strings
            if k in ('amount_to_pay', 'amount_paid') and v is not None:
                v = float(v)
            setattr(self, k, v)
        self.arrival_date_d   = parseDate(self.arrival_date)
        self.departure_date_d = parseDate(self.departure_date)
        if ratings:
            self.ratings = ratings
            self.contributor_level = meanRating(ratings['contributor_level'])
            self.requested_amount = meanRating(ratings['requested_amount'])
            self.mean_rating = (self.contributor_level + self.requested_amount) / 2.
            self.ratings_remarks = ratings['remarks']

        # we add some categories
        # standard rules

	if self.participant_category == None: self.participant_category = "--Category not selected--"

	self.cat_baf = self.participant_category in dc_config.CAT_BAF
	self.cat_bf  = self.participant_category in dc_config.CAT_BF
        self.cat_ba  = self.participant_category in dc_config.CAT_BA
        self.cat_b   = self.participant_category in dc_config.CAT_B
        self.cat_pro = self.participant_category in dc_config.CAT_PRO
        self.cat_cor = self.participant_category in dc_config.CAT_COR
        #
	self.cat_paid    = self.cat_pro or self.cat_cor
        self.cat_spons   = self.cat_baf or self.cat_bf or self.cat_ba
        self.cat_unspons = self.cat_b and not ( self.cat_paid or self.cat_spons )
        ### self.cat_none    = self.cat_paid
        self.cat_all     = self.cat_spons or self.cat_paid or self.cat_unspons
        self.cat_can_have_accom = self.cat_paid or self.cat_ba or self.cat_baf

        self.debcamp_no     = self.debcamp in dc_config.DEBCAMP_NO
        self.debcamp_plan   = self.debcamp in dc_config.DEBCAMP_PLAN
        self.debcamp_noplan = self.debcamp in dc_config.DEBCAMP_NOPLAN
        #
        self.debcamp_yes = self.debcamp_plan or self.debcamp_noplan
        self.debcamp_all = self.debcamp_yes or self.debcamp_no

        self.food_reg   = self.food in dc_config.FOOD_REG
        self.food_veg   = self.food in dc_config.FOOD_VEG
        self.food_vegan = self.food in dc_config.FOOD_VEGAN
        self.food_other = self.food in dc_config.FOOD_OTHER
        self.food_no    = self.food in dc_config.FOOD_NO
        #
        self.food_yes   = self.food_reg or self.food_veg or self.food_vegan or self.food_other

        self.accom_no  = self.accom in dc_config.ACCOM_NO
        self.accom_yes = self.accom in dc_config.ACCOM_YES

        self.status_spons   = self.status in dc_config.STATUS_SPONS
	self.status_nospons = self.status in dc_config.STATUS_NOSPONS
        #
        self.status_all     = self.status_spons or self.status_nospons

        self.debian_role_all = self.debian_role in dc_config.DEBIAN_ROLE_ALL

        self.debconf_role_spons   = self.debconf_role in dc_config.DEBCONF_ROLE_SPONS
        self.debconf_role_unspons = self.debconf_role in dc_config.DEBCONF_ROLE_UNSPONS
        #
        self.debconf_role_all = self.debconf_role_spons or self.debconf_role_unspons

        #
        # dc13
        #

	self.food_select_out    = self.food_select in dc_config.FOOD_OUTSIDE
	self.food_select_paid   = self.food_select in dc_config.FOOD_PAID
        self.food_select_spons  = self.food_select in dc_config.FOOD_SPONS

        self.accom_sponsored = self.accom in dc_config.ACCOM_SPONSORED
        self.accom_communal  = self.accom in dc_config.ACCOM_COMMUNAL
        self.accom_camping   = self.accom in dc_config.ACCOM_CAMPING
	self.accom_upgrade   = self.accom in dc_config.ACCOM_UPGRADE
        self.accom_eight     = self.accom in dc_config.ACCOM_EIGHT
        self.accom_two       = self.accom in dc_config.ACCOM_TWO

	if self.food_select_out or self.food_select_spons:
	    self.cost_food = 0
	else:
	    self.cost_food = dc_config.COST_FOOD_DAILY

        if   self.accom_sponsored:  self.cost_room = 0
        elif self.accom_communal:   self.cost_room = 20
        elif self.accom_camping:    self.cost_room = 20
	elif self.accom_upgrade:    self.cost_room = 20
        elif self.accom_eight:      self.cost_room = 30
        elif self.accom_two:        self.cost_room = 40
	elif self.accom_no:	    self.cost_room = 0
	else:			    self.cost_room = dc_config.COST_ROOM_DAILY

	if   self.cat_cor: self.fee = dc_config.COST_COR
	elif self.cat_pro: self.fee = dc_config.COST_PRO
	else:		   self.fee = 0

        # ovverides dc13

        self.cat_spons = self.accom_sponsored or self.accom_upgrade or self.food_select_spons
        self.cat_unspons = not self.cat_spons
        self.cat_all     = self.cat_spons or self.cat_paid or self.cat_unspons
        self.cat_can_have_accom = True


	self.accom_yes = self.accom_sponsored or self.accom_communal or self.accom_camping or self.accom_eight or self.accom_two or self.accom_upgrade
        # self.debcamp_yes = self.debcampdc13
        # self.debcamp_no = not self.debcampdc13
	
        if self.food_select_spons and self.accom_sponsored:
	    self.participant_category += "; want sponsored food and accommodation"
	elif self.food_select_spons:
	    self.participant_category += "; want sponsored food"
        elif self.accom_sponsored:
	    self.participant_category += "; want sponsored accommodation"

        # most of the data should be available, so last calculations:

        arrival_date, departure_date = self.effective_dates()
        self.days = (departure_date - arrival_date).days
        self.staying_days = self.days + 1

        self.total_food_costs = self.cost_food * self.days
        self.total_room_costs = self.cost_room * self.days
        self.total_fee_costs  = self.fee
        self.total_costs      = self.total_food_costs + self.total_room_costs + self.total_fee_costs


    def find_email(self):
        if self.email:
            return self.email
        c = db().cursor()
        c.execute("""
        select email
        from person
        where person_id=%(person_id)s;""", {'person_id':self.person_id})
        rows = c.fetchall()
        if len(rows) == 0: raise EmailError
        if len(rows) > 1:  raise EmailError
        email = rows[0][0]
        if email is None or email == "": raise EmailError
        return email
    def personHeader(self):
        return "(%4d) %s <%s>"%(self.person_id, self.name, self.email)
    def personInfo(self):
        lines = [ ]
        lines.append("Attend:         %s"%self.attend)
        lines.append("Reconfirmed:    %s"%self.reconfirmed)
        lines.append("Category:       %s"%self.participant_category)
        lines.append("Status:         %s"%self.status)
        lines.append("DebCamp:        %s"%self.debcamp)
        lines.append("Food:           %s"%self.food)
        lines.append("Accommodation:  %s"%self.accom)
        lines.append("Dates:          %s - %s"%(self.arrival_date, self.departure_date))
        return "\n".join(lines)
    def regSummary(self):
        if   self.cat_baf:  cat = "S"
        elif self.cat_bf:   cat = "BF"
        elif self.cat_ba:   cat = "BA"
        elif self.cat_b:    cat = "B"
        elif self.cat_pro:  cat = "PRO"
        elif self.cat_cor:  cat = "COR"
        else:               cat = "???"
        if   self.accom_yes: accom = "+A"
        elif self.accom_no:  accom = ""
        else:                accom = "??"
        if   self.food_reg:   food = "+F"
        elif self.food_veg:   food = "+FV"
        elif self.food_vegan: food = "+FVV"
        elif self.food_other: food = "+FO"
        elif self.food_no:    food = ""
        else:                 food = "???"
        if self.arrival_date is not None:
            arrive = self.arrival_date_d.strftime("%b%d")
        else: arrive = ""
        if self.departure_date is not None:
            depart = self.departure_date_d.strftime("%b%d")
        else:
            depart = ""
        if   self.debcamp_no:     debcamp = ""
        elif self.debcamp_plan:   debcamp = "+DC"
        elif self.debcamp_noplan: debcamp = "+DCnp"
        else: debcamp = "???"
        flags = ""
        if self.attend: flags += "A"
        else:           flags += " "
        if self.reconfirmed: flags += "R"
        else:                flags += " "

        return "%-3s %2s  %5s %5s  %-2s %-4s %-5s"%(
            cat, flags, arrive, depart, accom, food, debcamp)

    def _info(self):
        errors = [ "    "+str(e) for e in self.errors ]
        return "\n".join(errors)
        #print
        #for n in rownames:
        #    print "    %s: %s"%(n, getattr(self, n))
    def add_errors(self, Errors):
        # Test every error
         for e in Errors:
            try:
                self.errors.append(e(self))
            except NotAnError:
                pass
    def payment_items(self, nocustom=False):
        """

        `nocustom` argument makes this method not use self.amount_to_pay
        """
        items = [ ]
        # Figure out how many days they are staying:
        arrival_date, departure_date = self.effective_dates()
        num_days = (departure_date - arrival_date).days
        if num_days <= 7 \
               or (num_days <= 8 and arrival_date == DEBCONF_EARLIEST_ARRIVAL ):
            num_weeks = 1
        else:
            num_weeks = 2
        debcamp_days = (DEBCONF_START - arrival_date).days
        if debcamp_days < 0:
            debcamp_days = 0

        # Utility function for line-items
        def room(days):
            return ("Room for %s days"%days, COST_ROOM_DAILY*days)
        def roomDebcamp(days):
            return ("DebCamp room for %s days"%days, COST_ROOM_DAILY*days)
        def food(days):
            return ("Food for %s days"%days, COST_FOOD_DAILY*days)
        def foodDebcamp(days):
            return ("DebCamp food for %s days"%days, COST_FOOD_DAILY*days)

        # Actual items
        if self.participant_category in CAT_COR:
            if num_weeks == 2:
                items.append(("Corporate registration, DebCamp+DebConf",
                              2*COST_COR))
            else:
                items.append(("Corporate registration", COST_COR))

        elif self.participant_category in CAT_PRO:
            if num_weeks == 2:
               items.append(("Professional registration, DebCamp+DebConf",
                              2*COST_PRO))
            else:
                items.append(("Professional registration", COST_PRO))
        # Sponsored and DebCamp without a plan.
        elif self.participant_category in CAT_BAF|CAT_BA:
            if self.debcamp in DEBCAMP_NOPLAN:
                items.append((("DebCamp registration (without workplan)", COST_DEBCAMP)))
        # This is for the years we have pay-by-the-day as a default option.
        elif self.participant_category not in CAT_BAF|CAT_BA|CAT_PRO|CAT_COR:
            if self.accom in ACCOM_YES: #not in ACCOM_NO:
                items.append(room(num_days))
            #if self.food in FOOD_YES:
            #    items.append(food(num_days))

        # If the items above do not match amount_to_pay, then that
        # means the above logic is invalid via human override.  Thus,
        # we just call it a "custom registration".
        calculated_cost = sum(x[1] for x in items)
        if (self.amount_to_pay is not None
            and calculated_cost != self.amount_to_pay
            and not nocustom):
            items = [ ]
            items.append(("DebConf custom registration", self.amount_to_pay))


        #if self.participant_category not in CAT_BAF|CAT_PRO|CAT_COR \
        #       and self.debcamp in DEBCAMP_NOPLAN:
        #    items.append(("DebCamp registration", 650))
        #if self.participant_category not in CAT_COR|CAT_PRO|CAT_BAF|CAT_BA and\
        #      self.accom not in ACCOM_NO:
        #    items.append(("Room for %s days"%num_days, 53*num_days))
        #if self.participant_category not in CAT_COR|CAT_PRO|CAT_BAF|CAT_BF and\
        #      self.accom not in FOOD_NO:
        #    items.append(("Food for %s days"%num_days, 15*2*num_days))
        return items
    def payment_items_noFood(self):
        """Total attendee fees, excluding food.
        """
        return [x for x in self.payment_items()
                if "food" not in x[0].lower() ]

    def get_total_cost(self, nocustom=False):
        """Total attendee fees.

        See also the confusingly named total_cost() method and fix
        someday.
        """
        return sum(x[1] for x in self.payment_items(nocustom=nocustom))
    def get_total_cost_noFood(self):
        """Total attendee fees, excluding food.
        """
        return sum(x[1] for x in self.payment_items_noFood())


    def effective_dates(self):
        """Tuple of (arrival_date, departure_date), guessing if not provided."""
        arrival_date = self.arrival_date_d
        departure_date = self.departure_date_d
        if arrival_date is None:
            if self.debcamp_no:
                arrival_date = DEBCONF_START
            else:
                arrival_date = DEBCAMP_START
        if departure_date is None:
            departure_date = DEBCONF_END
        return arrival_date, departure_date
    def food_days(self):
        """Number of days this person eats our food."""
        arrival_date, departure_date = self.effective_dates()
        if self.food_no:
            return 0
        else:
            return (departure_date - arrival_date).days
    def food_cost(self):
        """Cost we have to pay for food for this person."""
        return self.food_days() * self.cost_food
    def accommodation_days(self):
        """Number of days this person is in our accommodations."""
        arrival_date, departure_date = self.effective_dates()
        if self.accom_no:
            return 0
        else:
            return (departure_date - arrival_date).days
    def accommodation_cost(self):
        """Cost we have to pay for accommodations for this person."""
        return self.accommodation_days() * self.cost_room
    def total_cost(self):
        """Total cost of this attendee to us, based on room+food usage.

        See also the confulingly named get_total_cost() method and fix
        someday.
        """
        return self.food_cost() + self.accommodation_cost()


get_people_by_id = {}

def get_people(where='dcvn.attend',
               set_amount_to_pay=False):
    import pentaconnect
    db = pentaconnect.get_conn()
    c = db.cursor()
    
    fields = ( 	'dcvn.person_id',
		'dcvn.name',
                'dcvn.first_name',
                'dcvn.last_name',
                'dcvn.nickname',
                'dcvn.public_name',
                'dcvn.email',
                'dcvn.gender',
                'dcvn.country',
                'dcvn.mime_type',
                'dcvn.file_extension',
                'dcvn.conference_id',
                'dcvn.arrived',
                'dcvn.event_role',
                'dcvn.role',
                'dcvn.reconfirmed',
                'dcvn.attend',
                'dcvn.accom',
                'dcvn.debcamp_option',
                'dcvn.person_type',
                'dcvn.debconf_role',
                'dcvn.debian_role',
                'dcvn.participant_category',
                'dcvn.food',
                'dcvn.food_select',
                'dcvn.camping',
                'dcvn.com_accom',
                'dcvn.debcampdc13',
		'truedcvn.arrival_date',
		'truedcvn.departure_date',
                'truedcvn.arrival_time',
                'truedcvn.departure_time',
		'truedcvn.debcamp',
		'truedcvn.debcamp_reason',
		'truedcvn.t_shirt_size',
		'truedcvn.total_cost',
		'truedcvn.cantfund',
		'truedcvn.status',
		'cpt.need_travel_cost',
		'dt.daytrip_option',
                'dccp.debianwork',
                'dccp.disabilities',
                'cp.arrived',
		'dccp.daytrip_id',
                'dccp.room_preference',
        )
    query = """SELECT %s
        FROM debconf.dc_view_find_person dcvn
        LEFT JOIN debconf.dc_view_numbers truedcvn ON (dcvn.person_id = truedcvn.id)
        LEFT JOIN conference_person cp ON (dcvn.person_id = cp.person_id AND dcvn.conference_id = cp.conference_id)
        LEFT JOIN debconf.dc_conference_person dccp ON (dcvn.person_id = dccp.person_id AND dcvn.conference_id = dccp.conference_id)
	LEFT JOIN conference_person_travel cpt ON (cp.conference_person_id = cpt.conference_person_id)
	LEFT JOIN debconf.dc_daytrip dt USING (daytrip_id)
        WHERE cp.conference_id = %s and MORE_WHERE
        ORDER BY dcvn.person_id
        ; """%(", ".join(fields), dc_config.conference_id)
    query = query.replace("MORE_WHERE", where)
    c.execute(query)
    people = dc_util.rowToDict(c.fetchall(), c)

    # Get person ratings
    c.execute("""
    select
        person_id, speaker_quality as contributor_level,
        competence as requested_amount, pr.remark
    from conference_person cp
        left join person_rating pr using (person_id)
    where cp.conference_id=%d
        and (speaker_quality is not null
             or competence is not null
             or pr.remark is not null)
    ;"""%dc_config.conference_id)
    ratings = dc_util.rowToDict(c.fetchall(), c)
    ratings_d = collections.defaultdict(lambda : {'requested_amount':[],
                                                  'contributor_level':[],
                                                  'remarks':[]})
    for row in ratings:
        for quality in ('contributor_level', 'requested_amount'):
            q = row[quality]
            if q is None: continue
            q -= 3
            q *= 50
            ratings_d[row['person_id']][quality].append(q)
        #    requested_amount  = row['requested_amount']
        #if requested_amount  is not None: requested_amount  -= 3
        #if contributor_level is not None: contributor_level -= 3
        #ratings_d[row['person_id']]['requested_amount'].append(requested_amount)
        #ratings_d[row['person_id']]['contributor_level'].append(contributor_level)
        if row['remark']:
            ratings_d[row['person_id']]['remarks'].append(row['remark'])

    import parse_attendee_payments


    payments = parse_attendee_payments.get_payments()


    People = [ ]
    for person in people:
        id = person['person_id']
        person = Person(ratings=ratings_d[id], **person)
        People.append(person)

        if payments is not None:
            # Set the amount they need to pay:
            if set_amount_to_pay and person.amount_to_pay is None:
                person.amount_to_pay = person.get_total_cost()

            # Set the amount they have paid:
            person.amount_paid = None
            person.promised_to_pay = False
            if id in payments:
                person.amount_paid = payments[id]['amount_paid']
                # Have they promised that they will pay, or made other arngmts?
                if payments[id].get('promise', False):
                    person.promised_to_pay = True


    for p in People:
	get_people_by_id[p.person_id] = p
    return People





def meanRating(v):
    """Mean rating, ignoring None values.
    """
    v = tuple(v)
    s = sum(x for x in v if x is not None)
    t = sum(1 for x in v if x is not None)
    if t == 0: return 0.
    return float(s) / t


class Talk(object):
    """Object to proxy talk objects

    Attributes on this object correspond with

    people - dict with keys Submitter, Speaker, etc, lists of those people for the talk
    ratings - ratings['rel'] is list of relevance ratings, and so on.
    rel - mean relevance
    act - mean actuality
    acc - mean acceptance
    rating - average rating = (rel+act+2*acc)/4
    """
    def __init__(self, talk, people=None, ratings=None, remarks=None):
        # talk - dict
        for name, value in talk.iteritems():
            setattr(self, name, value)
        self.people = people
        self.ratings = ratings
        self.remarks = remarks

        self.rel = meanRating(ratings['rel'])
        self.act = meanRating(ratings['act'])
        self.acc = meanRating(ratings['acc'])
        self.rating = (self.rel + self.act + 2*self.acc)/4.
    def __str__(self):
        return "Talk(event_id=%4d)"%self.event_id

def get_talks(where='e.conference_id=6'):
    import pentaconnect
    db = pentaconnect.get_conn()
    c = db.cursor()
    # Get talk data itself
    c.execute("""
    select
        event_id, title, subtitle, conference_track, event_type, submission_notes,
        event_state, event_state_progress, language, conference_room,
        start_time, resources, public, paper, slides,
        abstract, description, remark
    from event e
    where WHERE
    order by event_id
    ;""".replace('WHERE', where))
    talks = dc_util.rowToDict(c.fetchall(), c)
    
    # Get all the ratings
    c.execute("""
    select
        event_id, person_id as rater_id, relevance, actuality, acceptance, er.remark
    from event e
        left join event_rating er using (event_id)
    where WHERE
    ;""".replace('WHERE', where))
    ratings = dc_util.rowToDict(c.fetchall(), c)
    ratings_d = collections.defaultdict(lambda : {'rel':[], 'act':[], 'acc':[]})
    ratings_remarks_d = collections.defaultdict(list)
    for row in ratings:
        rel = row['relevance']
        act = row['actuality']
        acc = row['acceptance']
        if rel is not None: rel -= 3
        if act is not None: act -= 3
        if acc is not None: acc -= 3
        if rel is not None: rel *= 50
        if act is not None: act *= 50
        if acc is not None: acc *= 50
        ratings_d[row['event_id']]['rel'].append(rel)
        ratings_d[row['event_id']]['act'].append(act)
        ratings_d[row['event_id']]['acc'].append(acc)
        if row['remark']:
            ratings_remarks_d[row['event_id']].append(row['remark'])

    # Get all the speakers for the talks
    c.execute("""
    select
        event_id, event_role_name, person_id, name, p.email
    from event e
        left join view_event_person vep using (event_id)
        left join person p using (person_id)
    where WHERE
        and vep.translated='en'
    ;""".replace('WHERE', where))
    people = dc_util.rowToDict(c.fetchall(), c)
    people_d = collections.defaultdict(lambda : collections.defaultdict(list))
    for row in people:
        people_d[row['event_id']][row['event_role_name']].append(
                                         (row['person_id'], row['name'], row['email']))

    # Make events.
    Talks = [ ]
    for talk in talks:
        id = talk['event_id']
        Talks.append(Talk(talk, people=dict(people_d[id]),
                          ratings=dict(ratings_d[id]),
                          remarks=ratings_remarks_d[id]))
    return Talks
