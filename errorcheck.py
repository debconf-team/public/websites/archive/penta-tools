# Richard Darst, March 2010

import datetime
import textwrap
import time

import dc_objs
#from dc_objs import Person
from dc_util import parseDate, isNull, NotAnError
import dc_config

conference_id = 6

Errors = [ ]
Information = [ ]
class PentabarfError(object):
    _class = ( )  # classes of errors, such as "travel"
    def __str__(self):
        return self.msg%self.person.__dict__
    def __init__(self, person):
        self.person = person
        try:
            self.test(person)
        except AssertionError:
            return
        raise NotAnError
    def email(self):
        msg = """\
        %s missing error message:
          %s
        """%(self.__class__.__name__, self.msg)
        return msg%self.person.__dict__


##
## General information messages (level='info')
##

class RegistrationCheck(PentabarfError):
    level = 'info'
    def test(self, p):
        assert False
    msg = "Registration check"
    def email(self):
        return """\
        Here are your current registration details:

          Category requested:      %(participant_category)s
          Debian role:             %(debian_role)s
          DebConf role:            %(debconf_role)s
          Accommodation requested: %(accom)s
          Food requested:          %(food_select)s : %(food)s
          Reconfirmed:             %(reconfirmed)s

        Your currently listed arrival and departure dates at the venue are:

          Arrival date:            %(arrival_date)s
          Departure date:          %(departure_date)s
          Staying:                 %(staying_days)s days / %(days)s nights

        Your estimated fees (in Swiss Francs):
          food costs               %(total_food_costs)s CHF
          accommodation costs      %(total_room_costs)s CHF
          registration costs       %(total_fee_costs)s CHF

        Please ensure that these are correct, and if not fix them in
        Pentabarf immediately at
          https://penta.debconf.org/penta/submission/dc13/person
        """
Errors.append(RegistrationCheck)


class SponsorshipStatus(PentabarfError):
    level = 'info'
    def test(self, p):
        assert False
    msg = "Sponsorship status"
    #unsponsored_ids = open("dc10/sponsorship-not-yet-2010-06-01").read().split()
    #unsponsored_ids = [ int(x) for x in unsponsored_ids ]
    unsponsored_ids = [ ]
    def email(self):
        if self.cat_spons:
            self.msg = "Sponsorship NOT REQUESTED"
            return """\
        You haven't requested sponsorship - thank you for supporting
        DebConf by helping to secure your own funds.  You are:
          %(participant_category)s
        """
        elif self.person.person_id not in self.unsponsored_ids:
            self.msg = "Sponsorship GRANTED"
            return """\
        The DebConf team is pleased to confirm that we can offer
        accommodation and food sponsorship to everyone who requested
        it by 19 May.  You
        are:
          %(participant_category)s
        """
        else:
            self.msg = "Sponsorship PENDING"
            return """\
        The DebConf team is reviewing the attendees signed up for food
        or accommodation sponsorship.  At this time, we can't promise
        food and accommodation sponsorship to you. We will make
        sponsorship decisions as soon as we can.

        In order to help us make sponsorship decisions, could you
        please go to
           Penta -> Travel -> 'What are you doing for Debian?'
        and fill in details of your work relevant to Debian
        (on Debian itself or in the broader free software community).

        Again, be broad here!  We want as many interested community
        members to attend, so let us know what your other projects
        are.  If you are just interested in Debian, that's fine too,
        but please let us know your specific interests.

        If you are in any way connected to Debian, we'll do our best
        to make sure that you get sponsorship!

        If you are not granted sponsorship, you can still attend.  You
        will simply be asked to pay the actual costs of attendance.

        In addition, if you know of any organization which could help
        support DebConf financially, please contact
        sponsors@debconf.org -- we still need more funds to help make
        DebConf a better conference and to allow us to sponsor more
        people.
        """
#Errors.append(SponsorshipStatus)


class NewlyEnabledFieldsInfo(PentabarfError):
    level = 'info'
    def test(self, p):
        assert False
    msg = "Information on newly enabled fields"
    def email(self):
        return """\
        Some registration options have been recently enabled:

        * Reconfirmation - If you will attend DebConf13, you should
          confirm this in Pentabarf as soon as possible (by Sunday 30
          June at the latest).  Sometime before then please take a moment to
          decide whether you will be attending for sure.
            Penta -> General -> DebConf -> Reconfirm Attendance

        * Room preferences:
          For room information, please see
            http://debconf13.debconf.org/accommodation.xhtml
          and let us know what type of room you would like and any
          preferences for a roommate.
            Penta -> General -> DebConf -> Room Preference
        """
#Errors.append(NewlyEnabledFieldsInfo)


class DoubleCheckTravelSponsorshipNumbers(PentabarfError):
    """get them to verify numbers"""
    level = 'info'
    _class = ('travel',)
    def test(self, p):
        if p.need_travel_cost:
            raise AssertionError
    msg = "Your total travel cost is %(total_cost)s and you applied for travel sponsorship of %(cantfund)s."""
    def email(self):
        return """\
        Please double check your travel sponsorship numbers
          Total cost = %(total_cost)s
          Requested reimbursement = %(cantfund)s
        And if they are incorrect, please update them.
          Penta -> Travel -> Travel fare
          Penta -> Travel -> Amount I am unable to fund myself
        """
#Errors.append(DoubleCheckTravelSponsorshipNumbers)




##
## Error messages (level='info')
##


# 1) Entry for DC13 but no "I want to attend."
class NotMarkedAttend(PentabarfError):
    level = 'error'
    def test(self, p):
        assert p.attend
    msg = "Not marked attend (%(attend)s)."
    def email(self):
        return """\
        You have logged in during the DebConf13 registration period,
        but haven't marked yourself 'attend'.  This means we think you
        aren't attending DebConf.  This may be your intention, in
        which case, please ignore everything else in this email.
        You won't be mailed again.

        If you want to attend DebConf13, please mark it:
          Penta -> General -> I want to attend this conference
        """
Errors.append(NotMarkedAttend)


class NotMarkedReconfirmed(PentabarfError):
    level = 'error'
    def test(self, p):
        assert p.reconfirmed
    msg = "Not marked reconfirmed (%(reconfirmed)s)."
    def email(self):
        if self.person.cat_spons:
            self.msg = "Not marked reconfirmed, Sponsored (%(reconfirmed)s, %(participant_category)s)."
            return """\
        You have not yet reconfirmed that you will attend DebConf13.

        Since you are asking for sponsorship, you MUST reconfirm by 30
        June to let us know that you still plan to attend DebConf,
        otherwise your sponsorship will be cancelled.

        To reconfirm, please go to
          Penta -> General -> Reconfirm Attendance

        If you decide that you don't want to attend DebConf, please
        unselect 'I want to attend this conference'.
        """
        elif not self.person.accom_no:
            self.msg = "Not marked reconfirmed, Requests accom (%(reconfirmed)s, %(accom)s)."
            return """\
        You have not yet reconfirmed that you will attend DebConf13.

        Since you are asked us to arrange accommodation for you, you
        MUST reconfirm by 30 June to let us know that you still plan to
        attend DebConf.  Otherwise, we will cancel your accommodation.

        To reconfirm, please go to
          Penta -> General -> Reconfirm Attendance
        """
        else:
            return """\
        You have not yet reconfirmed that you will attend DebConf13.
        Please reconfirm by 30 June so that we can make accurate
        numbers.

        To reconfirm, please go to
          Penta -> General -> Reconfirm Attendance

        If you decide that you don't want to attend DebConf, please
        unselect 'I want to attend this conference'.
        """
Errors.append(NotMarkedReconfirmed)

class NotReconfirmedButWaitingForTravelSponsorship(PentabarfError):
    """Not reconfirmed and waiting for sponsorship - add a note"""
    level = NotMarkedReconfirmed.level
    def test(self, p):
        if not p.reconfirmed:
            #assert not p.need_travel_cost and not p.cantfund
            assert not p.need_travel_cost
    msg = "Not reconfirmed and requesting travel spons - note about if deadline comes.  (reconfirmed: %(reconfirmed)s, Needs travel: %(need_travel_cost)s, cantfund: %(cantfund)s)."
    def email(self):
        return """\
        ( If you are waiting to know if you'll be granted travel
          sponsorship or not, then please wait until you know for sure
          that you are coming before reconfirming -- if the deadline
          comes and you still don't know because of this, please send
          us a mail to let us know. )
        """
#Errors.append(NotReconfirmedButWaitingForTravelSponsorship)


class ReconfirmedAreYouReallyAttending(PentabarfError):
    level = 'info'
    def test(self, p):
        assert not p.reconfirmed
    msg = "Reconfirmed-still coming? (%(reconfirmed)s)."
    def email(self):
        return """\
        You are currently marked as reconfirmed.  Thank you for doing
        this promptly.  If you decide to not attend, please unmark
        attend and reconfirm fields:

          Penta -> General -> I want to attend this conference
          Penta -> General -> Reconfirm Attendance
        """
Errors.append(ReconfirmedAreYouReallyAttending)






class DayTripNotSelected(PentabarfError):
    level = 'info'
    def test(self, p):
        assert self.person.daytrip_option in ("Yes", )
    msg = "Not attending day trip (%(daytrip_option)s)"
    def email(self):
        return """\
        You are not currently marked as attending the day trip.
          Penta -> General -> Daytrip
        """
#Errors.append(DayTripNotSelected)










class MissingAttribute(PentabarfError):
    level = 'error'
    def test(self, p):
        assert not isNull(getattr(self.person, self.attrname))
    @property
    def msg(self):
        return "%s is not set (%%(%s)s)"%(self.attrname, self.attrname)

class NoArrivalDate(MissingAttribute):
    attrname = 'arrival_date'
    #def email(self):
    #    return """\
    #    You haven't marked an arrival date.  We know it is early and
    #    many people have not made final travel arrangements, but
    #    estimates for travel dates will still help us now.  Please
    #    enter you best estimate or desired arrival date, and update it
    #    once you know for sure.  There will be a reminder to
    #    double-check this date later, too.  Arrival time isn't as
    #    important now.
    #      Penta -> Travel -> 'Date of arrival'
    #    """
    def email(self):
        return """\
        You haven't marked an arrival date.  If you are staying in the
        DebConf accommodation, you MUST enter an arrival date by the
        reconfirmation deadline.  Even if you aren't staying in the
        DebConf accommodation, please fill this in to help our
        planning.  
          Penta -> Travel -> 'Date of arrival'
        """
class NoDepartureDate(MissingAttribute):
    attrname = 'departure_date'
    #def email(self):
    #    return """\
    #    You haven't marked a departure date.  We know it is early and
    #    many people have not made final travel arrangements, but
    #    estimates for travel dates will still help us now.  Please
    #    enter you best estimate or desired departure date, and update
    #    it once you know for sure.  There will be a reminder to
    #    double-check this date later, too.  Departure time isn't as
    #    important right now.
    #      Penta -> Travel -> 'Date of departure'
    #    """
    def email(self):
        return """\

        You haven't marked a departure date.  If you are staying in
        the DebConf accommodation, you MUST have a departure date by
        the reconfirmation deadline.  Even if you aren't staying in
        the DebConf accommodation, please fill this in to help our
        planning.
          Penta -> Travel -> 'Date of departure'
        """
Errors.extend((NoArrivalDate, NoDepartureDate))

# Arrival dates too early?
class ArrivalDateTooEarly(PentabarfError):
    level = 'critical'
    def test(self, p):
        d = parseDate(self.person.arrival_date)
        if not d: return
        assert d >= dc_objs.DEBCAMP_START
    msg = "Arrival date too early: %(arrival_date)s"
    def email(self):
        return """\
        You are arriving before Saturday 10 August.  DebConf accommodation
        will only be provided starting on 10 August, unless you have made
        special arrangements.
          Penta -> Travel -> 'Date of arrival'
        """
class DepartureDateTooLate(PentabarfError):
    level = 'critical'
    def test(self, p):
        d = parseDate(self.person.departure_date)
        if not d: return
        assert d <= dc_objs.DEBCONF_END
    msg = "Departure date too late: %(departure_date)s"
    def email(self):
        return """\
        You are departing after Sunday 18 August.  DebConf accommodation
        will only be provided until the morning of 18 August, unless you have
        made special arrangements.
          Penta -> Travel -> 'Date of departure'
        """
class DebcampWithPlanButPlanBlank(PentabarfError):
    """arriving during DebCamp"""
    level = 'error'
    def test(self, p):
        d = parseDate(self.person.arrival_date)
        if not d: return
        if d >= dc_objs.DEBCONF_EARLIEST_ARRIVAL: return #not arriving during camp
        if p.debcamp_plan:
            assert (p.debcamp_reason and p.debcamp_reason.strip() != '')
    msg = "Arrival date during DebCamp but no DebCamp work plan: %(arrival_date)s"
    def email(self):
        return """\
        You have an arrival date during DebCamp, but don't have a
        listed DebCamp work plan.  Please enter a DebCamp work plan or
        contact us about an early arrival.
          Penta -> General -> DebCamp -> DebCamp work plan
        """
        
class ArrivalDuringDebcampButNoDebcamp(PentabarfError):
    """Person arrival date during DebCamp, but not listed as DebCamp attending
    """
    level = 'error'
    def test(self, p):
        d = parseDate(self.person.arrival_date)
        if not d: return
        if d >= dc_objs.DEBCONF_EARLIEST_ARRIVAL: return #not arriving during camp
        assert p.debcamp_yes
    msg = "Arrival date during DebCamp but not DebCamp-registered: %(arrival_date)s"
    def email(self):
        return """\
        You have an arrival date during DebCamp, but are marked as not
        attending DebCamp.  Please look at your arrival date and
        DebCamp work plan, and see what needs to be adjusted.
          Penta -> General -> DebCamp section
          Penta -> Travel -> Arrival date
        """

Errors.extend((ArrivalDateTooEarly, DepartureDateTooLate,
               DebcampWithPlanButPlanBlank,
               ArrivalDuringDebcampButNoDebcamp))


# No arrival time
# No departure time

class CategoryPleaseSelectOne(PentabarfError):
    """Invalid participant_category (Basic, Professional, etc.)
    """
    level = 'error'
    def test(self, p):
        assert self.person.cat_all
    msg = "Participant category is %(participant_category)s"
    def email(self):
        return """\
        You don't seem to have a valid participant category:
          %(participant_category)s
        Please make sure that this is set.  Reload the page if you
        reset this, since this is a common recurring problem.  (Note:
        It is too late to select a sponsored category, and if you do so,
        it will be reverted.  The deadline for sponsorship has
        passed.)
          Penta -> General -> General section -> Category
        """
Errors.append(CategoryPleaseSelectOne)
#class StatusPleaseSelectOne(PentabarfError):
#    """Invalid status (DD, volunteer, etc)
#    """
#    level = 'error'
#    def test(self, p):
#        assert self.person.status_all
#    msg = "Participant debian status is %(status)s"
#    def email(self):
#        return """\
#        You don't seem to have a valid Debian status:
#          %(status)s
#        Please make sure that this is set.
#          Penta -> General -> General section -> Category
#        """
class DebianRolePleaseSelectOne(PentabarfError):
    """Invalid debian role (DD, etc)
    """
    level = 'error'
    def test(self, p):
        assert self.person.debian_role_all
    msg = "Participant Debian role is %(debian_role)s"
    def email(self):
        return """\
        You don't seem to have a valid Debian role:
          %(debian_role)s
        Please make sure that this is set.
          Penta -> General -> General section -> Debian role
        """
class DebconfRolePleaseSelectOne(PentabarfError):
    """Invalid DebConf role (Attendee, volunteer, etc)
    """
    level = 'error'
    def test(self, p):
        assert self.person.debconf_role_all
    msg = "Participant DebConf role is %(debconf_role)s"
    def email(self):
        return """\
        You don't seem to have a valid Debian status:
          %(debconf_role)s
        Please make sure that this is set.
          Penta -> General -> General section -> DebConf role
        """
#Errors.extend((CategoryPleaseSelectOne, StatusPleaseSelectOne))


class NoRoomPreference(PentabarfError):
    """Doesn't have a room preference listed
    """
    level = 'error'
    def test(self, p):
        assert p.accom_no \
               or not isNull(self.person.room_preference)
    msg = "Null room preference (%(room_preference)s, %(accom)s)"
    def email(self):
        return """\
        You haven't marked a room preference.  If you would like to
        request a particular roommate or type of room, please enter it
        here.  Roommate requests should be mutual.
          Penta -> General -> Room preference
        """
Errors.append(NoRoomPreference)




class SponsorshipTooMuch(PentabarfError):
    """Asking for sponsorship in a category that isn't sponsored."""
    level = 'critical'
    #no_sponsor_categories = dc_objs.CAT_UNSPONS
    #sponsored_statuses = dc_objs.DEBCONF_ROLE_SPONS
    def test(self, p):
        # If you aren't in a sponsored status, you better not be in a
        # sponsored category.
        if p.debconf_role_spons:
            assert p.cat_spons
    msg = "Participant requesting too much sponsorship (%(participant_category)s, %(debconf_role)s"
    def email(self):
        return """\
        Your registration status
          %(debconf_role)s
        means you aren't eligible for the level of sponsorship you are
        requesting
          %(participant_category)s
        This is odd, since this combination shouldn't be selectable
        under normal circumstances.  Please double-check the values of
        these fields and contact us if everything seems right.
          Penta -> General -> General section -> DebConf role
          Penta -> General -> General section -> Category
        """
#Errors.append(SponsorshipTooMuch)

# attend and no shirt selected
class NoTshirtSize(PentabarfError):
    """Warn people if they have not set a t-shirt size"""
    level = 'warn'
    def test(self, p):
        assert p.t_shirt_size != 'No Shirt selected'
    msg = "No t-shirt size selected (%(t_shirt_size)s)"
    def email(self):
        return """\
        You don't have a t-shirt size selected:
          %(t_shirt_size)s
        If you'd like one, please fix at
          Penta -> General -> General section ->  T-Shirt size
        """
Errors.append(NoTshirtSize)

class NoCountry(PentabarfError):
    """Warn people if they haven't specified a country."""
    level = 'warn'
    def test(self, p):
        assert p.country
    msg = "No country selected (%(country)s)."
    def email(self):
        return """\
        Please remember to fill in a country for demographic purposes.
          Penta -> Contact -> Address -> Country
        """
Errors.append(NoCountry)


# "Regular Room" and "Basic; want sponsored food" or
# "Regular Room" and "Basic; want sponsored accommodation" or

# however, if they request food sponsorship and indicate 'not eating
# with the group' that's inconsistent, 
class FoodSponsorshipButNotEatingWithGroup(PentabarfError):
    """Asking for food sponsorship but not eating with the group"""
    level = 'error'
    def test(self, p):
        if p.food_no:
            assert not p.food_select_spons
    msg = "Food sponsorship requested, but not eating with group (%(participant_category)s, %(food)s)"
    def email(self):
        return """\
        You have requested food sponsorship:
          %(participant_category)s
        but are listed as not eating with the group:
          %(food)s
        This is fine by us, but if this isn't what you want you can fix it:
          Penta -> General -> General section -> Category
          Penta -> General -> DebConf -> Food
        """
Errors.append(FoodSponsorshipButNotEatingWithGroup)

# ... and similarly if they request
# accommodation sponsorship and say they'll arrange their own
# accommodation
#class AccommodationSponsorshipButArrangingOwnAccommodation(PentabarfError):
#    """Asking for accommodation sponsorship but not requesting accom"""
#    level = 'error'
#    def test(self, p):
#        if p.participant_category in dc_objs.CAT_BAF|dc_objs.CAT_BA:
#            assert p.accom_yeS
#    msg = "Accommodation sponsorship requested, but not requesting a room (%(participant_category)s %(accom)s)"
#    def email(self):
#        return """\
#        You have requested sponsorship including accommodation:
#          %(participant_category)s
#        but are listed as not staying with the group:
#          %(accom)s
#        This is fine by us, but if it isn't what you want you can fix it:
#          Penta -> General -> General section -> Category
#          Penta -> General -> DebConf -> Accommodation
#        """
#Errors.append(AccommodationSponsorshipButArrangingOwnAccommodation)


# Warn about:
# 17:20 < Hydroxide> e.g., if they request professional or corporate
# but indicate that they don't want food or accommodation, we should
# just state that explicitly in some auto-mail
class PaidCategoryButNoFoodRequested(PentabarfError):
    """Paid category but not getting food sponorship"""
    level = 'warn'
    def test(self, p):
        if p.cat_paid:
            assert p.food_yes
    msg = "Paid category (%(participant_category)s) but no food requested (%(food)s).  This is okay, only a note."
    def email(self):
        return """\
        You are in a paid registration category
          %(participant_category)s
        but are listed as not eating with the group:
          %(food)s
        This is fine by us, but if it isn't what you want you can fix it:
          Penta -> General -> General section -> Category
          Penta -> General -> DebConf -> Food
        """
Errors.append(PaidCategoryButNoFoodRequested)
class PaidCategoryButNoAccommodationRequested(PentabarfError):
    """Paid category but not getting accom sponosrship"""
    level = 'warn'
    def test(self, p):
        if p.cat_paid:
            assert p.accom_yes
    msg = "Paid category (%(participant_category)s) but no accommodations requested (%(accom)s).  This is okay, only a note."
    def email(self):
        return """\
        You are in a paid registration category
          %(participant_category)s
        but are listed as not staying with the group:
          %(accom)s
        This is fine by us, but if it isn't what you want you can fix it:
          Penta -> General -> General section -> Category
          Penta -> General -> DebConf -> Accommodation
        """
#Errors.append(PaidCategoryButNoAccommodationRequested)

# Warn about:
# 17:21 < Hydroxide> or if they indicate that they do want food or
# accommodation without being sponsored for that, we should mention
# that they'll have to pay for the food or accommodation
class WillHaveToPayForFood(PentabarfError):
    """Requesting food but in a category that has to pay for it themselves"""
    level = 'info'
    def test(self, p):
        if p.cat_b or p.cat_ba:
            assert not p.food_no
    msg = "Category not including food (%(participant_category)s) food requested (%(food)s).  This is okay, only a note about payment."
    def email(self):
        return """\
        You are in a category that doesn't include food:
          %(participant_category)s
        but are listed as requesting food:
          %(food)s
          Penta -> General -> General section -> Category
          Penta -> General -> DebConf -> Food
        """
#Errors.append(WillHaveToPayForFood)
#class AccomNotAllowed(PentabarfError):
#    """Requesting accom but in a category that can't have it"""
#    level = 'warn'
#    def test(self, p):
#        if p.participant_category not in \
#               dc_config.CAT_CAN_HAVE_ACCOM:
#            assert p.accom not in dc_objs.ACCOM_YES
#    msg = "Category not including accom (%(participant_category)s) but accom requested (%(accom)s)."
#    def email(self):
#        return """\
#        You are in a category that doesn't include accommodation:
#          %(participant_category)s
#        but are listed as requesting accommodation:
#          %(accom)s
#
#        You shouldn't be able to select this, this is an error.  You
#        will be contacted about this if it is not fixed soon.  Please
#        fix one of the fields:
#          Penta -> General -> General section -> Category
#          Penta -> General -> DebConf -> Accommodation
#        """
#Errors.append(AccomNotAllowed)
#NoAccomAllowed = AccomNotAllowed
# Warn about:
# 17:21 < Hydroxide> and maybe even give a total cost in the case of
# accommodation, or total cost estimate per meal / per day / per stay
# for food

# Warning if not eating with group and will arrange own accom and no
# travel sponsorship



# 7) if they asked for travel sponsorship, ask them to double check
# the fields
# (see above)

# 6) People that asked for travel sponsorship but didn't enter any
# money
def xor(a, b):
    return (a and not b) or (not a and b)
class NeedTravelSponsorshipButNoAmountEntered(PentabarfError):
    """need_travel_cost set but cantfund is zero"""
    level = 'error'
    _class = ('travel',)
    def test(self, p):
        if p.need_travel_cost:
            assert p.cantfund and p.cantfund != 0 and p.total_cost and p.total_cost != 0
    msg = "Requesting travel sponsorship (%(need_travel_cost)s) but cantfund cost is empty (%(cantfund)s)."
    def email(self):
        return """\
        You are listed as requesting travel sponsorship, but didn't
        have travel cost or amount requested values entered:
          Total cost = %(total_cost)s
          Requested reimbursement = %(cantfund)s
        Unfortunately it's now too late to fill these in this year.
          Penta -> Travel -> Need help paying travel fare?
          Penta -> Travel -> Travel fare
          Penta -> Travel -> Amount I am unable to fund myself
        """

        """Please enter estimated travel costs and the amount of
        reimbursement you'd like.  We know you may not know your exact
        amount needed now, but an estimate is needed in order for us
        to allocate travel sponsorship money.  Remember, travel
        sponsorship isn't guaranteed.
        """
#Errors.append(NeedTravelSponsorshipButNoAmountEntered)

# if they entered money amounts, but haven't checked the "I need
# money" box.
class TravelCostNeededEnteredButNotMarkedNeedTravelCost(PentabarfError):
    """cantfund has a value but need_travel_cost not set"""
    level = 'warn'
    _class = ('travel',)
    def test(self, p):
        if p.cantfund and p.cantfund != 0:
            assert p.need_travel_cost
    msg = "Cantfund cost entered (%(cantfund)s) but not requesting travel sponsorship (%(need_travel_cost)s) ."
    def email(self):
        return """\
        You have a requested travel reimbursement entered, but not
        marked as needing travel sponsorship.
          Penta -> Travel -> Need help paying travel fare?
          Penta -> Travel -> Travel fare
          Penta -> Travel -> Amount I am unable to fund myself
        """
#Errors.append(TravelCostNeededEnteredButNotMarkedNeedTravelCost)

# cantfund >= total_cost
class TravelCantfundMoreThanTotalCost(PentabarfError):
    """Cantfund cost is more than total_cost"""
    level = 'error'
    _class = ('travel',)
    def test(self, p):
        if p.need_travel_cost:
            if p.cantfund is not None and p.total_cost is not None:
                assert p.cantfund <= p.total_cost
    msg = "Requesting more reimbursement (%(cantfund)s) than total cost (%(need_travel_cost)s)."
    def email(self):
        return """\
        You are listed as requesting more reimbursement than the total
        travel cost:
          Total cost = %(total_cost)s
          Requested reimbursement = %(cantfund)s
        Please make sure that these numbers are consistent.
          Penta -> Travel -> Travel fare
          Penta -> Travel -> Amount I am unable to fund myself
        """
#Errors.append(TravelCantfundMoreThanTotalCost)
# Requesting travel sponsorship but not accom sponosorship
class TravelCostNeededButNotRoom(PentabarfError):
    """cantfund has a value but need_travel_cost not set"""
    level = 'warn'
    _class = ('travel',)
    def test(self, p):
        if p.need_travel_cost:
            assert p.accom_yes
    msg = "Requesting travel sponsorship (%(need_travel_cost)s) but not requesting a room with us (%(accom)s)."
    def email(self):
        return """\
        You have requested travel reimbursement, but not a room with
        us.
          Penta -> Travel -> Need help paying travel fare?
          Penta -> General -> DebConf -> Accommodation
        """
#Errors.append(TravelCostNeededEnteredButNotMarkedNeedTravelCost)

"""  Sponsorship in the form of a room is easier to get, but
        we know there are cases where people can stay with friends or
        family for free.  Please make sure that these fields are what
        you really would like."""

# 13-year old age limit (DC10).  Tell us of this by reconfirmation deadline.
#class ThirteenYearOldAgeLimit(PentabarfError):
#    """Reminder about 13-year old age minimum
#    """
#    level = 'info'
#    def test(self, p):
#        assert False
#    msg = "13-year old age limit."
#    def email(self):
#        return """\
#        This warning applied to DebConf13.
#        """
#Errors.append(ThirteenYearOldAgeLimit)


##
## Checks for our use, but maybe shouldn't be seen by attendees.
##


class ReconfirmedButNotAttend(PentabarfError):
    """Person is reconfirmed but not marked attend."""
    level = 'warn'
    def test(self, p):
        assert not (p.reconfirmed and not p.attend)
    msg = "Reconfirmed but not attend. (%(attend)s, %(reconfirmed)s)."
    def email(self):
        return """\
        You are reconfirmed but not marked that you want to attend.
        Maybe you later found you couldn't attend so un-marked
        'attend' but still left 'reconfirmed'.  It would be a bit less
        ambiguous if you also unmarked 'reconfirmed'.  Or marked both,
        if that is your goal.

          Penta -> General -> I want to attend this conference
          Penta -> General -> Reconfirm Attendance
        """

class AccomAndReconfirmedButNoDates(PentabarfError):
    """Person is requesting accom but in B or BF or PSO."""
    level = 'error'
    def test(self, p):
        assert not (p.accom_yes
                    and p.reconfirmed
                    and not (p.arrival_date and p.departure_date))
    msg = "Reconfirmed+accom but missing a date (%(reconfirmed)s, %(accom)s, %(arrival_date)s, %(departure_date)s)."
    def email(self):
        return """\
        You are reconfirmed and requesting accommodations, but are
        missing either an arrival or departure date.  We can't reserve
        acccom without dates.  Fill in dates *now* or your
        accommodations will be canceled.

          Penta -> General -> Accommodation
          Penta -> Travel -> Date of arrival
          Penta -> Travel -> date of departure
        """

class AccomButNotReconfirmed(PentabarfError):
    """Person is requesting accom but in B or BF or PSO."""
    level = 'warn'
    def test(self, p):
        assert not (p.accom_yes
                    and not p.reconfirmed)
    msg = "Accom requested but not reconfirmed (%(reconfirmed)s, %(accom)s)."
    def email(self):
        return """\
        You are requesting accommodations but not reconfirmed.  We
        can't reserve a reserve you a room unless you reconfirm.
        Please fill in dates *now* or your accommodations will be
        canceled.

          Penta -> General -> Accommodation
          Penta -> Travel -> Date of arrival
          Penta -> Travel -> date of departure
        """

class SponsoredButNotAttend(PentabarfError):
    """Person is requesting accom but in B or BF or PSO."""
    level = 'warn'
    def test(self, p):
        assert not (p.cat_spons
                    and not p.attend)
    msg = "Accom requested but not reconfirmed (%(reconfirmed)s, %(accom)s)."
    def email(self):
        return """\
        You are requesting sponsorship but not attending.

          Penta -> General -> Category
          Penta -> General -> I want to attend this conference
        """
class SponsoredButNotReconfirmed(PentabarfError):
    """Person is requesting sponsorship but not reconfirmed"""
    level = 'warn'
    def test(self, p=None):
        assert not (self.cat_spons
                    and not self.reconfirmed)
    msg = "Accom requested but not reconfirmed (%(reconfirmed)s, %(accom)s)."
    def email(self):
        return """\
        You are requesting sponsorship but not reconfirmed.  If you
        don't reconfirm, yoour sponsorship will be canceled.  Please
        fill in dates *now*.

          Penta -> General -> Category
          Penta -> General -> Reconfirm attendance
        """




def get_rows():
    import pentaconnect
    db = pentaconnect.get_conn()
    c = db.cursor()

    fields = ('dcvn.id AS person_id',
              'dcvn.attend',
              'dcvn.reconfirmed',
              'dcvn.name',
              'dcvn.email',
              'dcvn.participant_category',
              'dcvn.status',
              'dcvn.arrival_date',
              'dcvn.departure_date',
              'dcvn.arrival_time',
              'dcvn.departure_time',
              'dcvn.food',
              'dcvn.accom',
              'p.gender',
              'dccp.room_preference',
              'p.country',
              'dcvn.debcamp',
              'dcvn.debcamp_reason',
              'dcvn.t_shirt_size',
              'dcvn.total_cost',
              'dcvn.cantfund',
              #'dcvn.total_cost_currency',
              #'dcvn.cantfund_currency',
              'cpt.need_travel_cost'
        )
    c.execute("""SELECT %s
        FROM debconf.dc_view_numbers dcvn
            LEFT JOIN person p on (id = person_id)
            JOIN conference_person cp USING (person_id)
            JOIN debconf.dc_conference_person dccp USING (person_id, conference_id)
            JOIN person USING (person_id)
            JOIN conference_person_travel cpt ON (cp.conference_person_id = cpt.conference_person_id)
        WHERE conference_id = %s /* and
            dcvn.attend */
        ORDER BY id
        ; """%(", ".join(fields), conference_id))
    rows = c.fetchall()
    rownames = [ x[0] for x in c.description ]
    people = [ dict(zip(rownames, row)) for row in rows ]
    return people

def getErrors(errors=None, excludeErrors=None,
              levels=None, excludeLevels=None,
              classes=None, excludeClasses=None):
    """Return all errors in this module according to certain criteria.

    Default is returning *everything* in this module, which is
    probably more than you want."""
    #for name, e in globals().items():
    #    print name, e, issubclass(e, PentabarfError), e == PentabarfError

    allErrors = [ e for (name,e) in globals().iteritems()
                  if (isinstance(e, type(PentabarfError))
                      and issubclass(e, PentabarfError)
                      and not e == PentabarfError
                      and not e == MissingAttribute)
                  ]
    allErrors = set(allErrors)
    # Filter by errors we want / errors to exclude
    if errors:
        allErrors = [ e for e in allErrors
                      if e.__name__ in errors ]
    if excludeErrors:
        allErrors = [ e for e in allErrors
                      if e.__name__ not in excludeErrors ]
    # Filter by levels we want / levels to exclude
    if levels:
        allErrors = [ e for e in allErrors
                      if e.level in levels ]
    if excludeLevels:
        allErrors = [ e for e in allErrors
                      if e.level not in excludeLevels ]
    # Filter by classes we want / classes to exclude
    if classes:
        allErrors = [ e for e in allErrors
                      if len(set(e._class)&set(classes)) > 0 ]
    if excludeClasses:
        allErrors = [ e for e in allErrors
                      if len(set(e._class)&set(excludeClasses)) == 0 ]


    return allErrors


def printPeopleErrors(people):
    """Print errors sorted by person"""
    for p in people:
        if len(p.errors) > 0:
            print '='*40
            print p.regSummary(), p.personHeader()
            print p._info()
def printErrorsByClass(people):
    """Print errors sorted by person"""
    import collections
    byError = collections.defaultdict(list)
    for p in people:
        for e in p.errors:
            byError[e.__class__.__name__].append(p)
    for errorName, ps in sorted(byError.iteritems()):
        print "="*40
        print errorName
        for p in ps:
            print p.regSummary(), p.personHeader()
        print
def printErrors(errors):
    """Given a list of errors, print the errors that are in the list,
    and those that aren't."""
    errors = set(errors)
    allErrors = set(getErrors())
    unknownErrors = set()
    print "Enabled errors:"
    for e in sorted(errors, key=lambda e:e.__name__):
        print "   ", e.__name__
    print "Disabled errors:"
    for e in sorted(allErrors-errors, key=lambda e:e.__name__):
        print "   ", e.__name__
    print "Errors unknown to the default full errors list:"
    for e in sorted(errors-allErrors, key=lambda e:e.__name__):
        print "   ", e.__name__


if __name__ == "__main__":

    errors = getErrors(excludeErrors=('NoRoomPreference',
                                      'NoCountry', ),
                       excludeLevels=('info', ))

    import os
    WHERE = os.environ.get('WHERE', 'dcvn.attend')
    print "Selection limit:", WHERE
    people = dc_objs.get_people(where=WHERE)

    printErrors(errors)
    print '\n'*5

    for p in people:
        p.add_errors(errors)
        #for e in p.errors:
        #    byError[e.__class__.__name__].append(p)


    print "Reconfirmed people:"
    printPeopleErrors(p for p in people if p.reconfirmed)
    print '\n'*15

    print "Not reconfirmed people:"
    printPeopleErrors(p for p in people if not p.reconfirmed)
    print '\n'*15

    print "People requesting accom:"
    printPeopleErrors(p for p in people if p.accom not in dc_objs.ACCOM_NO)
    print '\n'*15


    printErrorsByClass(people)
    #for errorName, ps in sorted(byError.iteritems()):
    #    print "="*40
    #    print errorName
    #    for p in ps:
    #        print p.regSummary(), p.personHeader()
    #    print
