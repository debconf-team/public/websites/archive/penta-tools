# Richard Darst, May2010

import collections
import time

import pentaconnect
import dc_util
import dc_objs

##  db = pentaconnect.get_conn()
##  
##  # Get all speakers names
##  c = db.cursor()
##  # Now, what's the speaker's name?
##  c.execute("""
##  select
##      event_id, title, subtitle, conference_track, event_type, submission_notes,
##      abstract, description
##  from event e
##  where e.conference_id=4
##  order by event_id
##  ;""")
##  talks = dc_util.rowToDict(c.fetchall(), c)
##  
##  
##  c.execute("""
##  select
##      event_id, person_id as rater_id, relevance, actuality, acceptance, er.remark
##  from event e
##      left join event_rating er using (event_id)
##  where e.conference_id=4
##  ;""")
##  ratings = dc_util.rowToDict(c.fetchall(), c)
##  ratings_d = collections.defaultdict(lambda : {'rel':[], 'act':[], 'acc':[]})
##  ratings_remarks_d = collections.defaultdict(list)
##  for row in ratings:
##      rel = row['relevance']
##      act = row['actuality']
##      acc = row['acceptance']
##      if rel is not None: rel -= 3
##      if act is not None: act -= 3
##      if acc is not None: acc -= 3
##      ratings_d[row['event_id']]['rel'].append(rel)
##      ratings_d[row['event_id']]['act'].append(act)
##      ratings_d[row['event_id']]['acc'].append(acc)
##      if row['remark']:
##          ratings_remarks_d[row['event_id']].append(row['remark'])
##  
##  c.execute("""
##  select
##      event_id, event_role_name, name
##  from event e
##      left join view_event_person vep using (event_id)
##  where e.conference_id=4
##      and vep.translated='en'
##  ;""")
##  speakers = dc_util.rowToDict(c.fetchall(), c)
##  speakers_d = collections.defaultdict(lambda : collections.defaultdict(list))
##  for row in speakers:
##      speakers_d[row['event_id']][row['event_role_name']].append(row['name'])



talks = dc_objs.get_talks(where='conference_id=4')

print time.ctime()
print

for talk in talks:
    print '='*20
    id = talk.event_id
    if talk.subtitle:
        print "%(title)s: %(subtitle)s"%talk.__dict__
    else:
        print "%(title)s"%talk.__dict__
    print "(%(event_id)s, %(event_type)s)"%talk.__dict__
    print "Submitters:", ", ".join(x[1] for x in talk.people.get('Submitter',[]))
    print "Speakers:", ", ".join(x[1] for x in talk.people.get('Speaker',[]))
    print "Relevance:", ", ".join(str(x) for x in sorted(talk.ratings['rel']))
    print "Actuality:", ", ".join(str(x) for x in sorted(talk.ratings['act']))
    print "Acceptance:", ", ".join(str(x) for x in sorted(talk.ratings['acc']))
    print "Averages R/A/A: %3.1f     %3.1f     %3.1f"%(talk.rel, talk.act, talk.acc)
    print "Rating-Overall: %3.1f"%talk.rating

    print
    print dc_util.wrap_text(talk.abstract)
    print
    print dc_util.wrap_text(talk.description)
    print
    print dc_util.wrap_text(talk.submission_notes)
    print
    print
    print "Remarks:"
    print
    for remark in talk.remarks:
        print dc_util.wrap_text(remark)
        print
    print
    print
    print


#from fitz import interactnow
