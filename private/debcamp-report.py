import collections

import dc_util
import dc_objs
#import pentaconnect
#db = pentaconnect.get_conn()
#c = db.cursor()

where = 'attend'

#c.execute("""select id, name, debcamp, participant_category, status,
#    arrival_date, departure_date
#from debconf.dc_view_numbers
#where %(where)s and
#    ( arrival_date<'2010-07-31' or debcamp !~ 'won''t be attending' )
#order by id
#;"""%locals())
#for type_, count in c.fetchall():
#    print "%3d  %s"%(count, type_)

import dc_objs
People = dc_objs.get_people(
    where="( dcvn.arrival_date<'2010-07-31' or debcamp !~ 'won''t be attending' )" )

print 'Conditions for being labeled with "audit":'
print '  (not a DD) and (not in a paid category) and (in our accommodations)'
print
print

for P in People:
    audit = False
    if (P.status != "Debian Developer"
        and P.participant_category not in dc_objs.CAT_B|dc_objs.CAT_PRO|dc_objs.CAT_COR
        and P.accom not in dc_objs.ACCOM_NO):
        audit = True
    print "="*20
    print "(%4d) %s (%s)%s"%(P.person_id, P.name, P.email, " (AUDIT)" if audit else '')
    print "Status: ", P.status
    print "Category: ", P.participant_category
    print "Accom: ", P.accom
    if True: # debcamp status (blocked for ease of reading only)
        if P.debcamp in dc_objs.DC_NOPLAN:
            print "DebCamp: No workplan"
        if P.debcamp in dc_objs.DC_PLAN:
            print "DebCamp: Has workplan"
        else:
            print "DebCamp:", P.debcamp
    print "Dates: %s - %s"%(P.arrival_date, P.departure_date)
    if "won't be attending" in P.debcamp:
        print "Person not attending DebCamp officially: arriving on", P.arrival_date
    # Should this be audited?
    if audit:
        print "Audit: True"
    print
    print dc_util.wrap_text(P.debcamp_reason)

    print
    print
    
