# Giacomo Catenazzi, July 2012

import os
import sys
import time
import datetime

import dc_config
from dc_config import *
import dc_objs
import dc_util

WHERE = os.environ.get('WHERE', 'dcvn.attend and dcvn.reconfirmed')
People = dc_objs.get_people(where=WHERE)

today_date = datetime.date.today()
tomorrow_date = today_date + datetime.timedelta(days=1)

today = today_date.strftime("%Y-%m-%d")

# check people


for p in People:
    trip = p.daytrip_option
    trip_id = p.daytrip_id
    if trip_id == None:
	continue
    print "%s,%s,%s,%s" % (p.person_id, today, trip_id, trip)
    


