# Richard Darst, April 2010

import collections
import csv
import datetime
import pgdb
import time

import dc_objs
import dc_util

#import pentaconnect
#db = pentaconnect.get_conn()
#c = db.cursor()
#
#c.execute("""select event_id, ve.title, ve.subtitle,
#                 ve.conference_track, ve.event_type, ve.description, ve.abstract,
#                 e.submission_notes
#             from view_event ve
#                 left join event e using (event_id)
#             where ve.conference_id=4 and ve.translated='en'
#             order by ve.event_id; """)
#
#rows = c.fetchall()
#rownames = [ x[0] for x in c.description ]
#data = [ dict(zip(rownames, row)) for row in rows ]
##print rownames
#
## Get speakers
#c.execute("""select name, event_id
#from event_person ep
#     join view_person p using (person_id) join event e using (event_id)
#where conference_id=4 and event_role='speaker'
#order by last_name;""")
#people = c.fetchall()
#speakers = collections.defaultdict(list)
#for name, event_id in people:
#    speakers[event_id].append(name)
## Get submitters
#c.execute("""select name, event_id
#from event_person ep
#     join view_person p using (person_id) join event e using (event_id)
#where conference_id=4 and event_role='submitter'
#order by last_name;""")
#people = c.fetchall()
#submitters = collections.defaultdict(list)
#for name, event_id in people:
#    submitters[event_id].append(name)


events = dc_objs.get_talks()


print time.ctime()
print
print

csv_rows = [ ]
f = open("dc10-talk-abstracts.csv", "w")
csvfile = csv.writer(f)

for event in events:
    if event.event_state != "accepted":
        continue
    abstract          = dc_util.wrap_text(event.abstract)
    desc              = dc_util.wrap_text(event.description)
    submmission_notes = dc_util.wrap_text(event.submission_notes)

    csvfile.writerow((event.title,
                      event.subtitle,
                     ", ".join("%s <%s>"%(p[1],p[2])
                               for p in event.people.get('Speaker', [])),
                     ", ".join("%s <%s>"%(p[1],p[2])
                               for p in event.people.get('Submitter', [])),
                     ", ".join("%s <%s>"%(p[1],p[2])
                               for p in event.people.get('Coordinator', [])),
                      event.event_state,
                      event.event_state_progress,
                      event.conference_track,
                      event.event_type,
                      event.public,
                      event.paper,
                      event.slides,
                      event.abstract,
                      event.description,
                      event.event_id))
    
    print '='* 20
    if event.subtitle:
        print "%(title)s: %(subtitle)s"%event.__dict__
    else:
        print event.title
    print "(%(event_id)s, %(event_type)s)"%event.__dict__
    print "Submitters:", ", ".join("%s <%s>"%(p[1],p[2])
                                   for p in event.people.get('Submitter', []))
    print "Speakers:  ", ", ".join("%s <%s>"%(p[1],p[2])
                                   for p in event.people.get('Speaker', []))
    print "State:     ", event.event_state
    print "Track:     ", event.conference_track
    print
    print "Abstract:"
    print abstract
    print
    print
    print "Description:"
    print desc
    print
    print

    
