import collections
import time

import dc_util
import dc_objs
#import pentaconnect
#db = pentaconnect.get_conn()
#c = db.cursor()

where = 'attend'

#c.execute("""select id, name, debcamp, participant_category, status,
#    arrival_date, departure_date
#from debconf.dc_view_numbers
#where %(where)s and
#    ( arrival_date<'2010-07-31' or debcamp !~ 'won''t be attending' )
#order by id
#;"""%locals())
#for type_, count in c.fetchall():
#    print "%3d  %s"%(count, type_)

print time.ctime()
print

import dc_objs

people = dc_objs.get_people()

print "Total attendees:", len(people)

people = [ p for p in people
           if p.participant_category in dc_objs.CAT_BAF|dc_objs.CAT_BA|dc_objs.CAT_BF ]
print "Limiting above to people with any form of sponsorship:", len(people)

#people = [ p for p in people
#           if '@debian.org' not in p.email ]
#print "Limiting above to people without @debian.org in their email address:",len(people)

#people = [ p for p in people
#           if p.country in ('ve', 'sv', 'mx', 'ec') ]
#print "Limiting above to people from ve, sv, mx, ec:",len(people)


print
print "Printing report for only the people meeting both of those criteria"
print

for P in people:
    print "="*20
    print "(%4d) %s (%s)"%(P.person_id, P.name, P.email)
    print "Status:   ", P.status
    print "Category: ", P.participant_category
    print "Accom:    ", P.accom
    print "Food:     ", P.food
    if True: # debcamp status (blocked for ease of reading only)
        if P.debcamp in dc_objs.DC_NOPLAN:
            print "DebCamp:   No workplan"
        if P.debcamp in dc_objs.DC_PLAN:
            print "DebCamp:   Has workplan"
        else:
            print "DebCamp:", P.debcamp
    print "Dates: %s - %s"%(P.arrival_date, P.departure_date)
    print

    print dc_util.wrap_text(P.debianwork)
    print

    if P.debcamp_reason:
        print "Debcamp Reason:"
        print
        print dc_util.wrap_text(P.debcamp_reason)
        print
    print
    print
    
