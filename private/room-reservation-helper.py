# Richard Darst, July 2011
# -*- encoding: utf-8 -*-

'''Room reservations script.

How to use:
* Put "end" in the first cell after the last row of the first sheet.
* Make sure headers exist on the top row.  These are automatically detected

make sure the penta/ directory is in your PYTHONPATH.  I run like this:
  cd debconf-data/penta/
  PYTHONPATH="$PYTHONPATH:."  # if needed
  python private/room-reservation-helper.py

arguments are:
* first argument, spreadsheet
* second and further arguments, report to output:
** reservations
** totals
** full_list
** debug

python room-reservation-helper.py ~/dc13rooms.ods reservations totals
'''

import collections
import datetime
import mailbox
import os
import sys
import re

reload(sys)
sys.setdefaultencoding('utf-8')

def parse_date(x):
    dt = datetime.datetime.strptime(x, '%Y-%m-%d')
    return dt.date()

class Person(object):
    """This is person.  people have arrival and depart dates"""
    def __init__(self, name, arrive, depart, email=None, pentaid=None, typ=None):
        self.name = name
        self.arrive = arrive
        self.depart = depart
        self.email = email
        self.pentaid = pentaid
        self.typ = typ
    @property
    def nights(self):
        """How many days is this room in use?"""
        return (self.depart-self.arrive).days
    @property
    def arrive_s(self):
        """Arrival date as a string"""
        return self.arrive.strftime("%d %B")
    @property
    def depart_s(self):
        """Departure date as a string"""
        return self.depart.strftime("%d %B")
        

class Room(object):
    """Represents one room, can contain multiple people in it,
    possibly arriving on different dates"""
    def __init__(self, name, type=None, people=None):
        self.name = name
        self.type = type
        if people is None:
            self.people = [ ]
        else:
            self.people = people
    @property
    def arrive(self):
        """First date this room is used - test all people"""
        if not self.people:
            return None
        return min(p.arrive for p in self.people)
    @property
    def depart(self):
        """Last date this room is used - fir"""
        if not self.people:
            return None
        return max(p.depart for p in self.people)
    @property
    def arrive_s(self):
        """Arrival date as a string"""
        return self.arrive.strftime("%d %B")
    @property
    def depart_s(self):
        """Departure date as a string"""
        return self.depart.strftime("%d %B")
    @property
    def nights(self):
        """How many nights is this room in use?"""
        return (self.depart-self.arrive).days
    @property
    def personNights(self):
        return sum(((p.depart-p.arrive).days) for p in self.people)
    @property
    def n(self):
        """How many people in this room?"""
        return len(self.people)
    @property
    def list(self):
        return ", ".join("%s: %s" % (p.pentaid, p.name) for p in self.people)
    @property
    def full_list(self):
        return "".join("  %s-%s: %2s:%4s %s \n" % (p.arrive_s, p.depart_s, p.typ, p.pentaid, p.name) for p in self.people)
    def add(self, person):
        self.people.append(person)

class Hotel(object):
    def __init__(self, name=None):
        self.name = name
        self._rooms = { }
    def add(self, room, person):
        """Add a person in a room"""
        roomname = room.name.lower() # lower case room names
        if roomname not in self._rooms:
            self._rooms[roomname] = room
        self._rooms[roomname].add(person)

    def summary(self):
        """How many rooms of what type needed per day?
        """
        counts = collections.defaultdict(int)
        for room in self._rooms.values():
            key = (room.arrive, room.depart, room.type, room.n)
            counts[key] += 1
        for key in sorted(counts):
            count = counts[key]
            arrive, depart, roomtype, n = key
            #print "%2s x %-10s room for %s   from %s to %s"%(
            #    count, roomtype, n,
            #    arrive.strftime("%d %B"), depart.strftime("%d %B"), )
            print "%1s x %s room from %s to %s"%(
                count, roomtype,
                arrive.strftime("%d %B"), depart.strftime("%d %B"), )
    def debug(self):
        rooms = list(self._rooms.values())
        rooms.sort(key=lambda x: (x.arrive, x.type))
        for r in rooms:
            print "%s room from %s to %s (%s) [%s]"%(r.type, r.arrive_s, r.depart_s, r.list, r.name)
        print
        print "Totals:"
        types = collections.defaultdict(int)
        for r in rooms:
            types[r.type] += 1
        for t, count in types.items():
            print "%s room x %s"%(t, count)
    def totals(self):
        rooms = list(self._rooms.values())
        types = collections.defaultdict(int)
        for r in rooms:
            types[r.type] += 1
        for t, count in sorted(types.items()):
            print "%s room x %s"%(t, count)
        print

        totalRooms = sum(types.values())
        assert totalRooms == len(rooms)
        totalRoomNights = sum(r.nights for r in rooms)
        totalPersonNights = sum(r.nights*r.n for r in rooms)
        realPersonNights = sum(r.personNights for r in rooms)
        price = prices[self.name]
        print "Total rooms:", totalRooms
        print "Total room-nights:", totalRoomNights
        print "Total person-nights (overestimate):", totalPersonNights
        print "Real person-nights (overestimate):", realPersonNights
        print "Price per person-night:", price
        print "Total cost:", totalPersonNights*price
        print "Real total cost:", realPersonNights*price
    def reservations(self):
        """How many rooms of what type needed per day?
        """
        rooms = self._rooms.values()
        rooms.sort(key=lambda r: (r.name, r.arrive))

        for r in rooms:
            print "Room %-10s  %s -- %s   %-10s  (for %s person)"%(
                r.name, r.arrive_s, r.depart_s, '('+r.type+')', r.n)
    def assignments(self):
        """How many rooms of what type needed per day?
        """
        rooms = self._rooms.values()
        rooms.sort(key=lambda r: (r.name, r.arrive))

        for r in rooms:
            print "Room %-10s  %s -- %s   %-10s  (%s)"%(
                r.name, r.arrive_s, r.depart_s, '('+r.type+')', r.list)
    def full_list(self):
        """Description of every room data
        """
        rooms = self._rooms.values()
        rooms.sort(key=lambda r: (r.name, r.arrive))
        for r in rooms:
            print " ".join(room_descr.get(self.name + "-" + r.name, ("???", self.name + "-" + r.name)))
            print "Room %-10s  %s -- %s   %-10s\n%s"%(
                r.name, r.arrive_s, r.depart_s, '('+r.type+')', r.full_list)
                
    def mail_people(self):
        """
        """
        from emailutil import Email, Message
        import emailutil
        

        rooms = self._rooms.values()
        rooms.sort(key=lambda r: (r.name, r.arrive))

        for r in rooms:
            for p in r.people:
                name = p.name
                pentaid = p.pentaid
                roomName = r.name
                hotel = self.name
                arrive = p.arrive_s
                depart = p.depart_s
                nights = p.nights
                roomType = r.type.lower()
                if len(r.people) == 1:
                    roommates = "You have a single room."
                else:
                    #print id(p), [id(rm) for rm in r.people]
                    roommates = [ rm for rm in r.people if rm is not p ]
                    roommates = "\n".join(
                       "  %s (from %s to %s)"%(rm.name, rm.arrive_s, rm.depart_s)
                        for rm in roommates)
                    roommates = "Your roommates will be:\n"+roommates
                # Make the mail itself
                this_body = mail_body%locals()
                this_body = this_body.encode('utf-8').replace("Seminole2", "Seminole").replace("Seminolex", "Seminole")
                RegTeam = Email("DebConf Registration Team",
                                "registration@debconf.org")
                if not p.email:
                    print "Person has no email address:", p.name
                To = Email(p.name, p.email)
                msg = Message(From=RegTeam, Cc=RegTeam,
                              To=To,
                              Body=this_body,
                              Subject="DebConf13: Room assignments")
                mbox.add(str(msg))


prices ={
    'DebConf': 0.0,
    'DebCamp': 0.0,
    'unassigned': 0.0,
    }

def parse_spreadsheet(fname):
    import readsxc
    f = readsxc.OOSpreadData(fname)

    hotels = { }

    names = None
    numpeople = 0
    for row in f:
        # Automatically create columns
        if not names:
            names = [ n.lower().replace(' ', '_').replace('-', '_')
                      for n in row ]
            continue
        if not row or row[0].lower() == "end" or (not row[0] and not row[1]):
            break
        numpeople += 1
        # Row is a dict of row labels
        row = dict(zip(names, row))
        #print row
	#print row['person_id']

        # Ignore empty names (free bed)
        if not row.has_key('name') and not row.has_key('person_id'):
            continue

        # Get room information
        roomName = row['room']  # row['room_alloc']
        if not roomName.startswith('n--'):
            roomType = re.findall('^(^.*[^0-9])[0-9]*$', roomName)[0][0]
            if roomType == "Dis": roomType = "Disabled: " + roomType
            room = Room(roomName, type="CONF-"+roomType)
        else:
	    roomName = ""

        roomNameCamp = row['roomcamp']
        if roomNameCamp:
	    roomTypeCamp = re.findall('^(.*[^0-9])[0-9]*$', roomNameCamp)[0][0]
            if roomTypeCamp == "Dis": roomTypeCamp = "Disabled: " + roomTypeCamp
            roomCamp = Room(roomNameCamp, type="CAMP-"+roomTypeCamp)

        # Get the hotel
        #hotelName = row['hotel'].capitalize()
        #if not hotelName: hotelName = 'unassigned'
        hotelName = "DebConf"
        if hotelName not in hotels:
            hotels[hotelName] = Hotel(name=hotelName)
        hotelNameCamp = "DebCamp"
        if hotelNameCamp not in hotels:
            hotels[hotelNameCamp] = Hotel(name=hotelNameCamp)
        hotel = hotels[hotelName]
        hotelCamp = hotels[hotelNameCamp]

        if not roomName and not roomNameCamp:
	    continue
        arrive = parse_date(row['arrival_date'])
        depart = parse_date(row['departure_date'])
        person = Person(name=row['name'], arrive=arrive, depart=depart,
                        email=row['email'], pentaid=row['person_id'], typ=row['hide'])

        if roomName: 
	    hotel.add(room=room, person=person)
	if roomNameCamp:
            hotelCamp.add(room=roomCamp, person=person)
    print "People processed:", numpeople
    return hotels

room_descr = {}
def code_to_description():
    for line in open("cate.penta/dc13/room.names").readlines():
	hotel, code, building, floor, room_number, size, comment = line.strip().split(None, 6)
        room_descr[hotel + "-" + code] = (hotel, code, building, floor, room_number, size, comment)

mail_body = u"""\
Dear %(name)s,

This message is to confirm that we have reserved accommodation for you
during DebConf.

You will stay in Hotel %(hotel)s.  The room number will be provided
later, probably only at hotel check-in.

You should check in on the afternoon/evening of %(arrive)s and leave
the room on the morning of %(depart)s.

%(roommates)s

Please note that you will be responsible yourself for any costs in the
hotel which are not included in the basic room rate.



Here are the details for the DebConf hotel:

Hotel Seminole
De Bancentro carretera a Masaya 1c oeste 1c al sur
Managua, Managua
Nicaragua
+(505) 2270-0061
http://www.seminoleplaza.com/


If you see something wrong in this email, please reply immediately to
the registration team (this address).

Thank you,

- the DebConf registration team.

-- 
id: %(pentaid)s
"""




if __name__ == "__main__":
    # This should only be done for "mail_people" command, will fix later.
    mbox = mailbox.mbox("dc13roommails.mbox")
    mbox.clear() ; mbox.flush()
    
    hotels = parse_spreadsheet(sys.argv[1])
    code_to_description()
    print
    for hotelname, hotel in sorted(hotels.items()):
#        if hotelname == "Seminole":
#            continue
        print "Hotel:", hotelname
        for cmd in sys.argv[2:]:
            getattr(hotel, cmd)()
            print
        print

