# Richard Darst, April 2010

import os
import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


body_sponsored = """\

Dear %(name)s,

It's not too late!
We still have a few 4-bed rooms available for an upgrade fee of
CHF20/day.  Or, if you'd rather stay in a 8-bed room with your
friends, they're available for the same fee.  Both upgrade options
have bedding. The non-upgraded communal accommodations require a
sleeping bag.

Please contact registration@debconf.org to upgrade as soon as
possible or if you have any questions.

Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

body_selfpaid_communal = """\

Dear %(name)s,

You registered for 'self-paid communal accommodation'.
We do still have some smaller rooms left if you'd like to upgrade.
The <8-bed rooms cost CHF10/day more than what you pay now.
If you'd prefer a 4-bed room, please mention this in the comment field
and we'll try to put you into a 4-bed rooms if available.

Please contact registration@debconf.org to upgrade as soon as possible
or if you have any questions.

Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""


body_selfpaid_8beds = """\

Dear %(name)s,

You selected 'self-paid accommodation in room with up to 8 beds', we
are happy to offer you a free upgrade to a 4 beds rooms.

If you take this opportunity (the free upgrade), please contact us at
registration@debconf.org and/or update your room preference in the
conference registration system at 
<https://penta.debconf.org/penta/submission/dc13/person> as soon as
possible.

Please contact registration@debconf.org  if you have any questions.

Thank you,

--
The DebConf team
(person_id: %(person_id)4d)
"""




WHERE = os.environ.get('WHERE', 'dcvn.attend AND dcvn.reconfirmed')
People = dc_objs.get_people(where=WHERE)
#[ P.add_errors(Errors) for P in People ]
mbox = mailbox.mbox("dc13upgrade2.mbox")
mbox.clear() ; mbox.flush()

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


for person in People:
    # This parts deals with printing.
    AllErrors = person.errors

    # hack - need to regen string messages first.
    [ e.email() for e in person.errors ]

    # This part deals with mailing.
    info   = [ e for e in AllErrors if e.level=='info' ]
    errors = [ e for e in AllErrors if e.level!='info' ]
    info   = make_items(info)
    errors = make_items(errors)
    if not errors.strip(): errors = "    (none)"

    if person.accom_sponsored:
        Body = body_sponsored % person.__dict__
        subject = "DebConf room upgrade [%d]" % person.person_id
    elif person.accom_communal or person.accom_camping:
        Body = body_selfpaid_communal % person.__dict__
	subject = "DebConf - upgrade to a smaller room? [%d]" % person.person_id
    elif person.accom_eight:
	Body =  body_selfpaid_8beds % person.__dict__
	subject = "DebConf - free upgrade to a smaller room? [%d]" % person.person_id
    else:
	continue

    Body = Body.replace("Accomodation", "Accommodation")
    Body = Body.replace("accomodation", "accommodation")
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject=subject,
        Body=Body,
        extraMsgid="dc13upgrade2",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))


print
