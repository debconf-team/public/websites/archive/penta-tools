#Where: dcvn.attend
#Subject: DebConf13 confirmation of attendance

We are now starting the confirmation period for DebConf13. It is 
important for us to know final numbers soon, so unfortunately if you 
don't confirm your attendance, and fill in your travel dates, before 
the deadline (Sunday 30 June), you will not be counted for food,
accommodation,  t-shirts, proceedings, etc. Also, you will not be
eligible for food or accommodation sponsorship.

If you have registered as a Professional or Corporate attendee, we 
also ask you to please make the corresponding payment during this 
period. Other attendees are welcome to make a donation towards the 
costs of DebConf, if possible. See below.

== Instructions ==

To confirm that you will attend DebConf13, you need to log into your 
Pentabarf account <https://penta.debconf.org/penta/submission/dc13/person>, 
and tick the "Reconfirm attendance" box. You must also fill in the 
dates during which you will be present at the conference. Please also 
look through the other data, and fill in anything you missed earlier,
or correct the data if your plans have changed.

For our records, please remember to fill in your country, thank you!

You must have arrival and departure dates filled in, or you won't be 
counted. Please tell us the times you expect to arrive at the venue, 
and when you plan to leave the conference. If you want to 
arrange group travel to/from the conference, use the 
<http://wiki.debconf.org/wiki/DebConf13/Arrivals> 
travel coordination page but please still fill in your final arrival 
times in Pentabarf so we know when to expect you, and when to have rooms
ready for those staying in the sponsored accommodation.

Please, *PLEASE*, do not confirm your attendance unless you know for 
sure that you are coming to DebConf13. If you confirm but then don't 
come, this will be counted against you for the next DebConf's 
sponsorship decisions. Of course, if you confirm and end up not being 
able to come, cancelling soon is much better than not telling us.

If you are waiting to know if you'll be granted travel sponsorship or 
not, then please wait until you know for sure that you are coming 
before confirming -- if the deadline comes and you still don't know 
because of this, please send us a mail to let us know.

The confirmation period ends on Friday 15 June, but please confirm 
*today* if you already know that you will attend!

== Room preferences ==

In Penta you will find a "Room preference" box which will allow you to
enter whom you would like to share a room with or you would like to be
near. Even if you don't have any particular preferences, please state
if you smoke/don't smoke, snore/don't snore, etc.

== Attendance fee and donations ==

If you are attending as a Professional or Corporate attendee, or are
attending DebCamp without a specific work plan, we ask you to pay your
attendance fee before Friday 15 June.

We also encourage anyone who wants to help make DebConf a success to
make a donation towards the costs of the conference. You can choose to
register as a Professional, or make a donation of any size.

You can make payment in EUR or CHF (through debian.ch) by bank transfer
or USD  (through SPI) using a credit card. There are also other payment 
options available. Be sure to select the right category for the 
payment (attendance fee or donation).
Check http://debconf13.debconf.org/payments.xhtml

If you have any doubts regarding how much you need to pay, please mail
registration@debconf.org.

== Current registration details ==

All registered attendees will soon be sent a personal message with
their current registration details.  Please check these carefully!

If you have any questions, please contact us via:
* IRC: #debconf-team on irc.oftc.net (real-time questions)
* or mailing list: debconf-team@lists.debconf.org (publically archived)
* or non-public email: registration@debconf.org

The registration system URL is https://penta.debconf.org/penta/submission/dc13/person

The notes below each item give hints on where to look in our
registration for the relevant fields for each item.
Please make sure that everything is correct, and that you tick
"Reconfirm attendance" if appropriate, by the deadline of Sunday 30 June
(but preferably today).

If you make any changes, log out and back in to verify that your changes were committed!

We look forward to seeing you soon in Switzerland,

The DebConf team

