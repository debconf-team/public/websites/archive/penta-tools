#!/bin/bash

set -e
cd `dirname $0`
#export location=lefschetz:www/http/debconf/
#export location=mire:www/rkd.zgib.net/http/debconf/
export location=alioth:/home/groups/debconf-data/htdocs/stats/

export WHERE=dcvn.attend
python public/registration-statistics.py > published-data/registration-data.txt
#python public/rooms-statistics.py > published-data/rooms-by-date.txt #obselete
python public/rooms-statistics2.py > published-data/rooms-by-date-2.txt
#python public/talks-statistics.py > published-data/talks-data.txt
python public/food-statistics.py > published-data/food-by-date.txt
#python public/payment-statistics.py > published-data/payment-data.txt
#python public/budget-report.py > published-data/budget-report.txt
unset WHERE

for info in \
    "only-all/	true	conference_id=5" \
    "only-attend/	dcvn.attend	conference_id=5" \
    "only-reconfirmed/	dcvn.attend and dcvn.reconfirmed	conference_id=5" \
    "only-arrived/	dcvn.attend and dcvn.reconfirmed and cp.arrived	conference_id=5" \
    ; do
    prefix="`echo "$info" | cut -f 1`"
    export WHERE="`echo "$info" | cut -f 2`"
    #export CONFERENCE_ID=`echo "$info" | cut -f 3`
    mkdir -p published-data/${prefix}
    
    python public/registration-statistics.py > published-data/${prefix}registration-data.txt
#    python public/payment-statistics.py > published-data/${prefix}payment-data.txt
    python public/rooms-statistics2.py > published-data/${prefix}rooms-by-date-2.txt
    python public/food-statistics.py > published-data/${prefix}food-by-date.txt
    python public/budget-report.py > published-data/${prefix}budget-report.txt
    unset prefix
    unset WHERE
done

# For all conferences:
for info in \
    "dc12-	6	conference_id=6" \
    "dc11-	5	conference_id=5" \
    "dc10-	4	conference_id=4" \
    "dc09-	3	conference_id=3" \
    "dc08-	2	conference_id=2" \
    "dc07-	1	conference_id=1" ; do
    confprefix=`echo "$info" | cut -f 1`
    export CONFERENCE_ID=`echo "$info" | cut -f 2`
    export WHERE=`echo "$info" | cut -f 3`
    
    python public/talks-statistics.py > published-data/${confprefix}talks-data.txt
    unset CONFERENCE_ID
    unset WHERE
done

# Accounting updates
LEDGER="ledger -f ../debconf-data/dc12/ledger-dc12"

#if [ -e $LEDGER ] ; then
cat > published-data/accounting-balances.txt <<EOF
Current balance sheet:

`$LEDGER bal --no-color`



Current balance sheet, converted to EUR:

`$LEDGER bal -X EUR --no-color`



Our current bank balances:

`$LEDGER bal --no-color ^assets`



Our incomes so far (negative):

`$LEDGER bal --no-color ^incomes`



Our expenses so far:

`$LEDGER bal --no-color ^expenses`



Reimbursements expected:

`$LEDGER bal --no-color ^liabilities`

EOF
#fi


rsync -raiv published-data/ $location
#rsync -raiv published-data/ alioth:/srv/home/groups/debconf-data/htdocs/stats/

# this contains hidden filenames
test -e publish-private-dat.sh && sh publish-private-dat.sh

