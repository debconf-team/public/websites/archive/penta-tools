# Richard Darst, May 2010

import collections
import os
import time


import pentaconnect
db = pentaconnect.get_conn()
c = db.cursor()

WHERE = os.environ.get('WHERE', "dcvn.attend")
where = WHERE
from dc_config import conference_id

print "Selection limit:", where
print time.ctime()

print
print
print "="*20
print "Registered/confirmed/etc"
c.execute("""select count(id) from debconf.dc_view_numbers;""")
print "%3d Total accounts in system (this conference)"%c.fetchall()[0][0]
c.execute("""select count(id) from debconf.dc_view_numbers where attend;""")
print "%3d attend"%c.fetchall()[0][0]
c.execute("""select count(id) from debconf.dc_view_numbers where reconfirmed;""")
print "%3d reconfirmed"%c.fetchall()[0][0]
c.execute("""select count(id) from debconf.dc_view_numbers left join conference_person cp on (id=person_id)
             where arrived
             and conference_id=%(conference_id)d;"""%locals())
print "%3d arrived (doesn't work yet)"%0


print
print
print "="*20
print "Debian roles:"
c.execute("""select role_in_debian, count(role_in_debian)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by role_in_debian
order by -count(role_in_debian);"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print "="*20
print "DebConf roles:"
c.execute("""select role_in_debconf, count(role_in_debconf)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by role_in_debconf
order by -count(role_in_debconf);"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print "="*20
print "DebConf roles by Debian roles:"
c.execute("""select count(id), role_in_debconf, role_in_debian
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by role_in_debconf, role_in_debian
order by role_in_debconf, role_in_debian;"""%locals())
for count, x, y in c.fetchall():
    print "%3d  %-20s  %-20s"%(count, str(x)[:20], str(y)[:20])


print
print
print "="*20
print "Participant categories:"
c.execute("""select participant_category, count(participant_category)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by participant_category
order by -count(participant_category);"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print "="*20
print "Registered/confirmed/etc (DebCamp)"
c.execute("""select count(id) from debconf.dc_view_numbers dcvn where debcamp !~ 'won''t be attending';""")
print "%3d Total accounts in system (this conference)"%c.fetchall()[0][0]
c.execute("""select count(id) from debconf.dc_view_numbers dcvn where attend and debcamp !~ 'won''t be attending';""")
print "%3d attend"%c.fetchall()[0][0]
c.execute("""select count(id) from debconf.dc_view_numbers dcvn where reconfirmed and debcamp !~ 'won''t be attending';""")
print "%3d reconfirmed"%c.fetchall()[0][0]
c.execute("""select count(id) from debconf.dc_view_numbers left join conference_person on (id=person_id) where arrived and conference_id=%(conference_id)d;"""%locals())
print "%3d arrived (doesn't work yet)"%0


print
print
print "="*20
print "DebCamp categories (DebCamp):"
c.execute("""select debcamp, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by debcamp
order by -count(debcamp);"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print
print "="*20
print "Debian roles (DebCamp):"
c.execute("""select role_in_debian, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s and
    debcamp !~ 'won''t be attending'
    and conference_id=%(conference_id)d
group by role_in_debian
order by -count(role_in_debian);"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print
print "="*20
print "DebConf roles (DebCamp):"
c.execute("""select role_in_debconf, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s and
    debcamp !~ 'won''t be attending'
    and conference_id=%(conference_id)d
group by role_in_debconf
order by -count(role_in_debconf);"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)

print
print
print "="*20
print "DebConf roles by Debian roles:"
c.execute("""select count(id), role_in_debconf, role_in_debian
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and debcamp !~ 'won''t be attending'
    and conference_id=%(conference_id)d
group by role_in_debconf, role_in_debian
order by role_in_debconf, role_in_debian;"""%locals())
for count, x, y in c.fetchall():
    print "%3d  %-20s  %-20s"%(count, str(x)[:20], str(y)[:20])


print
print
print "="*20
print "Participant categories (DebCamp):"
c.execute("""select participant_category, count(id)
from debconf.dc_view_numbers dcvn
    left join conference_person cp on (id=person_id)
where %(where)s and
    debcamp !~ 'won''t be attending'
    and conference_id=%(conference_id)d
group by participant_category
order by -count(participant_category);"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print "="*20
print "Gender (True=M, False=F):"
c.execute("""select gender, count(gender)
from debconf.dc_view_numbers dcvn
    left join conference_person cp on (id=cp.person_id)
    left join person p on (id=p.person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by gender
order by -count(gender);"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print "="*20
print "Gender  (True=M, False=F) (DebCamp):"
c.execute("""select gender, count(gender)
from debconf.dc_view_numbers dcvn
    left join conference_person cp on (id=cp.person_id)
    left join person p on (id=p.person_id)
where %(where)s
    and conference_id=%(conference_id)d
    and debcamp !~ 'won''t be attending'
group by gender
order by -count(gender);"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print "="*20
print "Accommodation:"
c.execute("""select accom, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by accom
order by -count(accom);"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print "="*20
print "Accommodation (DebCamp):"
c.execute("""select accom, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
    and debcamp !~ 'won''t be attending'
group by accom
order by -count(accom);"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print "="*20
print "Food:"
c.execute("""select food, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by food
order by -count(food);"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print "="*20
print "Food (DebCamp):"
c.execute("""select food, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
    and debcamp !~ 'won''t be attending'
group by food
order by -count(food);"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print "="*20
c.execute("""select count(id), participant_category, accom
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by participant_category, accom
order by participant_category, accom;"""%locals())
for count, x, y in c.fetchall():
    print "%3d  %-40s  %-15s"%(count, str(x)[:40], str(y)[:15])
print
print
print "="*20
c.execute("""select count(id), participant_category, food
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by participant_category, food
order by participant_category, food;"""%locals())
for count, x, y in c.fetchall():
    print "%3d  %-40s  %-15s"%(count, str(x)[:40], str(y)[:15])
print
print
print "="*20
c.execute("""select count(id), participant_category, accom, food
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by participant_category, food, accom
order by participant_category, accom, food;"""%locals())
for count, x, y, z in c.fetchall():
    print "%3d  %-40s  %-15s  %-15s"%(count, str(x)[:40], str(y)[:15], str(z)[:15])


print
print
print "="*20
print "Number of days attending:"
c.execute("""select days, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by days
order by -count(days);"""%locals())
counts = collections.defaultdict(int)
counts[0] # make it non-empty so max() won't fail
days_None = 0
for days, count in c.fetchall():
    if days is None:
        days_None = count
        continue
    days = - int(days)
    counts[days] += int(count)
for days in range(max(counts), -1, -1):
    print "%2d  %2d"%(days, counts[days])
print "missing a date  - ", days_None

print
print
print "="*20
print "Arrival date:"
c.execute("""select arrival_date, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by arrival_date
order by arrival_date;"""%locals())
counts = collections.defaultdict(int)
days_None = 0
for date, count in c.fetchall():
    print "%10s  %3d"%(date, count)
print
print
print "="*20
print "Departure date:"
c.execute("""select departure_date, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by departure_date
order by departure_date;"""%locals())
counts = collections.defaultdict(int)
days_None = 0
for date, count in c.fetchall():
    print "%10s  %3d"%(date, count)


print
print
print "="*20
print "Country:"
c.execute("""select country, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by country
order by -count(country), country;"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print "="*20
print "T-shirt sizes:"
c.execute("""select t_shirt_size, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by t_shirt_size
order by t_shirt_size;"""%locals())
for type_, count in c.fetchall():
    print "%3d  %s"%(count, type_)


print
print
print "="*20
print "T-shirt sizes, by DebConf role:"
c.execute("""select t_shirt_size, role_in_debconf, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by t_shirt_size, role_in_debconf
order by role_in_debconf, t_shirt_size;"""%locals())
for type_, status, count in c.fetchall():
    print "%3d  %-20s %-s"%(count, type_, status)


print
print
print "="*20
print "T-shirt sizes, by Debian role:"
c.execute("""select t_shirt_size, role_in_debian, count(id)
from debconf.dc_view_numbers dcvn left join conference_person cp on (id=person_id)
where %(where)s
    and conference_id=%(conference_id)d
group by t_shirt_size, role_in_debian
order by role_in_debian, t_shirt_size;"""%locals())
for type_, status, count in c.fetchall():
    print "%3d  %-20s %-s"%(count, type_, status)
