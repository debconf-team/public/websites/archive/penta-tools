# Richard Darst, June 2010

import collections
import os
import re
import time

import dc_objs

print time.ctime()
print
print


WHERE = os.environ.get('WHERE', "dcvn.attend")

allPeople = dc_objs.get_people(where=WHERE)

stateRE = re.compile('(?P<city>[^,])+(?:,| ) *(?P<state>[a-z]{2})(?: *[0-9]{5})? *$', re.I)
state2_RE = re.compile('(?P<city>[^,])+, *(?P<state>.*?) *(?:[0-9]{5})?', re.I)

states = collections.defaultdict(int)

for P in allPeople:
    if P.country != 'us':
        continue
    if P.city is None:
        continue
    
    m = stateRE.search(P.city)
    if m:
        #print P.city, m.group('state')
        state = m.group('state').upper()
        states[state] += 1
        continue
    #m = state2_RE.search(P.city)
    #if m:
    #    print m.group('state')
    print P.city
print "Number of states:", len(states)
print
print
for state in sorted(states):
    print "%3d %2s"%(states[state], state)
print
print
for count, state in sorted(((c,s) for s,c in states.iteritems()),
                           reverse=True):
    print "%3d %2s"%(count, state)

    

